function [density,drhodp,drhodT,drhodN] = cubicEquationOfState(p,T,eos,varargin)


assert(all(size(p)==size(T)), 'input matrices/vectors must be of the same dimension!')

compDer = (nargout>1);

% R = 8.3145;  % universal specific molar gas constant
R = 8.3144621;

d = eos.components.d; % mixing parameters
N = eos.components.ratios; % molar ratios
M = eos.components.Mass; % molar masses
Tc = eos.components.Tc; % critical temperatures
pc = eos.components.pc; % critical temperatures

eosparms = eos.parameters; % parameters of cubic equation of state

if (nargin>3)
    moles = varargin{1};
else
    moles = 1;
end

[Z,dZdp,dZdT,dZdN] = solve(p,T,N,moles,pc,Tc,R,d,eosparms,compDer);

kg = dot(N,M);

density = (kg*p)./(Z .* ((moles*R).*T)); 

if (isempty(dZdp) || isempty(dZdT))
    drhodp = []; drhodT = []; drhodN = [];
else
    drhodp = density.*(1./p - dZdp./Z);
    drhodT = -density.*(1./T + dZdT./Z);
    drhodN = cell(1,numel(N));
    for ni = 1:numel(N)
        drhodN{ni} = -density.*(dZdN{ni}./Z + 1./moles);
    end
end


%==========================

    function [Z,dZdp,dZdT,dZdN] = solve(p,T,N,moles,pc,Tc,R,d,eos,compDer)
        numc = numel(N);
        a0 = cell(1,numc);
        da0dT = cell(1,numc);
        for nu = 1:numc
            Tr  = T./Tc;
            alpha = 1+ eos.kappa .* (1 - Tr.^0.5 );
            a0{nu} = eos.omegaA .* ((R*Tc(nu)) .* alpha).^2 ./ pc(nu);
            if compDer
                dalphadT = -(eos.kappa / 2) .* (1 ./ (Tc(nu) * T).^0.5);
                da0dT{nu} = 2 * (a0{nu} ./ alpha) .* dalphadT;
            end
        end
        
        a = 0.*T; % initialize
        for i = 1:numc
            for j = 1:numc
                sqrta0a0 = (a0{i} .* a0{j}).^0.5;
                a1 = (1-d(i,j)) .* sqrta0a0;
                a = a + (N(i)*N(j)) .*  a1;
            end
        end
        b0 = eos.omegaB * R  * (Tc./pc);
        b  = dot(N,b0);

        RT = R.*T;
        A = (a.*p) ./ (RT.^2);
        B = (b.*p) ./ RT; 
        
       Z = computeZfactor(A,B,eos.delta1,eos.delta2);
        
        dZdp = []; dZdT = []; dZdN = []; % initialize derivatives
        
        if compDer
            dadT = 0.*T;
            for i = 1:numc
                for j = 1:numc
                    sqrta0a0 = (a0{i} .* a0{j}).^0.5;
                    da1dT = (da0dT{i}.*a0{j} + a0{i}.*da0dT{j}) ./ (2 * sqrta0a0);
                    dadT = dadT + ((1-d(i,j))*N(i)*N(j)).*da1dT;
                end
            end
            dAdp = A ./ p;
            dBdp = B ./ p;
            dAdT = -2 * (A ./ T) + (A ./ a) .* dadT;
            dBdT = -B ./ T;

            dZdp = dZdX(Z,a1, a2, A, B, dAdp, dBdp,eos.delta1,eos.delta2);
            dZdT = dZdX(Z,a1, a2, A, B, dAdT, dBdT,eos.delta1,eos.delta2);
            dZdN = cell(1,numc);
            dadN = 2 * (sum(N) - a) ./ moles;
            for nu = 1:numc
                dbdN = (b0{nu} - b) ./ moles;
                dAdN = (A ./ a) .* dadN;
                dBdN = (B ./ b) .* dbdN;
                dZdN{nu} = dZdX(Z,a1, a2, A, B, dAdN, dBdN,eos.delta1,eos.delta2);
            end
        end
        
    end

      function  Z  = computeZfactor(A,B,delta1,delta2)

          a0 = -A .* B  -  (delta1 * delta2).*((B.^2) .* (1 + B)) ;
          a1 = A + (delta1 + delta2).*(B .* (1 + B)) + (delta1 * delta2) .* (B.^2);
          a2 = -(1 + delta1 + delta2).* B -  1;
          
          [zMin,zMid,zMax] = CubicEquation(a0, a1, a2);
          % choose right root
          Z = zMax;
          assert(all(Z(:)>0),'non-positive compressibility factor')
      end

    function [zMin,zMid,zMax] = CubicEquation(a0, a1, a2)
        Q = (3 * a1 - a2.^2) / 9.;
        Rs = (9 * a1 .* a2 - 27 * a0 - 2 * a2.^3) / 54.;
        D = Q.^3 + Rs.^2;
        
        only = (D > 0 | Q == 0);
        zMin = 0.*a0;
        zMid = zMin;
        zMax = zMin;
        if (any(only))   % One real root
            Sarg = Rs(only) + D(only).^0.5;
            Targ = Rs(only) - D(only).^0.5;
            S = sign(Sarg) .* (abs(Sarg)).^(1. / 3.);
            Ts = sign(Targ) .* (abs(Targ)).^(1. / 3.);
            z0 = -a2(only) / 3. + S + Ts;
            zMin(only) =  z0;
            zMax(only) = z0;
            zMid(only) = z0;
        end
        if (~all(only(:))) % Three real roots
            disp('multiple roots for cubic eos')
            theta = acos(Rs(~only) ./ ((-Q(~only).^3).^0.5));
            factor = 2 * (-Q(~only)).^0.5;
            a2o3 = a2(~only)/3;
            z1 = factor .* cos(theta / 3.) - a2o3;
            z2 = factor .* cos((theta + 2 *pi) / 3.) - a2o3;
            z3 = factor .* cos((theta + 4 * pi) / 3.) - a2o3;
            z = sort([z1,z2,z3],2);
            zMin(~only) = z(:,1);
            zMid(~only) = z(:,2);
            zMax(~only) = z(:,3);
        end
    end

    function Y = dZdX(Z,a1, a2, A, B, dAdX, dBdX,delta1,delta2)
        da0dX = -dAdX * B - dBdX ...
                * (A + B * (2 + 3 * B) * delta1 * delta2);
        da1dX = dAdX + dBdX ...
                * ((1 + 2 * B) * (delta1 + delta2) + 2 * delta1* delta2 * B);
        da2dX = -dBdX * (1 + delta1 + delta2);
        Y = -(da2dX * Z * Z + da1dX * Z + da0dX) ...
                / (3 * Z * Z + 2 * a2 * Z + a1);
    end


end

