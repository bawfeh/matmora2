function [y, dydx1, dydx2, dydvi] = interp3DPVT(T, x1, x2, flag,vi)
compDer = (nargout>1);

yl = zeros(size(x1));
yr = zeros(size(x1));
if compDer
    dydx1l = zeros(size(x1));
    dydx1r = zeros(size(x1));
    dydx2l = zeros(size(x1));
    dydx2r = zeros(size(x1));
    dwdvi   = zeros(size(x1));
end


v = T.key;
lims = v; lims(1)=-inf; lims(end)=inf;
[bin,bin] = histc(vi, lims);                                     %#ok
w = (vi-v(bin))./(v(bin+1)-v(bin));
if compDer
    dwdvi = 1./(v(bin+1)-v(bin));
end
for tn = 1:numel(v)
    tab{1} = T.data{tn};
    
    ixl = (bin==(tn-1));
    ixr = (bin==tn);
      
    [yl(ixl)] = interpRegPVT(tab, x1(ixl), x2(ixl), flag(ixl),':');
    [yr(ixr)] = interpRegPVT(tab, x1(ixr), x2(ixr), flag(ixr),':');
    if compDer
        [yl(ixl), dydx1l(ixl), dydx2l(ixl)] = interpRegPVT(tab, x1(ixl), x2(ixl), flag(ixl), ':');
        [yr(ixr), dydx1r(ixr), dydx2r(ixr)] = interpRegPVT(tab, x1(ixr), x2(ixr), flag(ixr), ':');
        
    end   
    
end

y = yl.*w + yr.*(1-w);
if compDer
    dydx1 = dydx1l.*w + dydx1r.*(1-w);
    dydx2 = dydx2l.*w + dydx2r.*(1-w);
    dydvi = (yl-yr).*dwdvi;
end
end
