function [krW,krO,krG] = CB2Relperm(krw,krnw,sw,sg)

krW = sw.^2;
krO = krw(1-sw-sg);
krG = krnw(sg);