function [krw,krnw,Pcnw] = CoreyBrooks(Pe,sr,lambda)

load('PcTable.mat','sat','Pc')
P = Pe*Pc;
Pcnw = @(s) interpTable(sat, P, s);


sw_w = @(s) eff_saturations(s, sr, 1);
sw_nw = @(s) eff_saturations(s, sr, 2);

krw = @(s) sw_w(s) .^ ( ( 2 + 3 *lambda)/lambda );
krnw = @(s) (sw_nw(s)).^2 .* (1-(1-sw_nw(s)).^ ( ( 2 + lambda)/lambda ));
% Pcnw = @(s) Pe * sw_w(s).^(-1/lambda);


function se = eff_saturations(s, sr,ind)
   den = 1 - sum(sr);
   se  = ( s - sr(ind) ) ./ den;  se(se < 0) = 0;  se(se > 1) = 1;
