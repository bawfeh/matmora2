function [yi,dyi] = interp1Table(X, Y, xi, varargin)

assert(nargin>=3,'interp1 requires at least 3 input arguments!');
assert(all(size(X)==size(Y)), 'interp1 data must be of the same dimension!');

i = zeros(size(xi));
w = zeros(size(xi));
for l = 1:numel(xi)
    i(l) = getIndex(X, xi(l));
    w(l) = getWeight(X,i(l),xi(l));
end

yi = (1-w) .* Y(i+1) + w .* Y(i);
    
if (nargout==1)
    dyi = [];
    return;
end

if  (nargout==2)
    dyi = (Y(i+1) -  Y(i))./(X(i+1)-X(i));
end



    function i =  getIndex(X, x) 
        N = numel(X);
        if (x <= X(1))
            i = 1;
            return;
        elseif ( x >= X(N-2) )
                i  = N-2;
            return;
        end

        first = 1;   last = N - 1;
        % The usual binary search
        while (first <= last) 
            mid = floor((first + last) / 2);
            if (X(mid) < x)
                first = mid + 1;
            else
                last = mid - 1;
            end
        end
        i = first - 1;
    end

    function xw = getWeight(X, index,x)
        Xmin = X(index); Xmax = X(index+1);
        if (x <= Xmin)
            xw = 0;
            return;
        elseif (x >= Xmax)
            xw = 1;
            return;
        else
            xw =  (x - Xmin) / (Xmax - Xmin);
        end
    end

end