function [yi, dyidxi, dyidvi] = interp2DPVT_new(T, xi, vi, reginx)
compDer = (nargout>1);
nreg = numel(reginx);

w   = zeros(size(xi));
yil = zeros(size(xi));
yir = zeros(size(xi));
if compDer
    dyidxil = zeros(size(xi));
    dyidxir = zeros(size(xi));
    dwdvi   = zeros(size(xi));
end

for k = 1:nreg
    v = T{k}.key;  pos = T{k}.pos;
    lims = v; lims(1)=-inf; lims(end)=inf;
    vi_k = vi(reginx{k});
    vi_k = full(vi_k);  % bawfeh
    [bin,bin] = histc(vi_k, lims);                                     %#ok
    w(reginx{k}) = (vi_k-v(bin))./(v(bin+1)-v(bin));
    if compDer
        dwdvi(reginx{k}) = 1./(v(bin+1)-v(bin));
    end
    for tn = 1:numel(v)
        lns = pos(tn):(pos(tn+1)-1);
        tab = T{k}.data(lns,:);

        ixl = (bin==(tn-1));
        ixr = (bin==tn);
        if reginx{k} ~= ':'
            ixl = ixl.*reginx{k};
            ixr = ixr.*reginx{k};
        end
        xi = full(xi);  % bawfeh
%         if compDer
%             [yil(ixl),dyidxil(ixl)] = interp1Table(tab(:,1), tab(:,2), xi(ixl),compDer);
%             [yir(ixr),dyidxir(ixr)] = interp1Table(tab(:,1), tab(:,2), xi(ixr),compDer);
%         else
%             yil(ixl) = interp1Table(tab(:,1), tab(:,2), xi(ixl));
%             yir(ixr) = interp1Table(tab(:,1), tab(:,2), xi(ixr));
%         end
        yil(ixl) = interpTable(tab(:,1), tab(:,2), xi(ixl));
        yir(ixr) = interpTable(tab(:,1), tab(:,2), xi(ixr));
        if compDer
%             if (size(tab,2)>2)
%                 dyidxil(ixl) = interpTable(tab(:,1), tab(:,3), xi(ixl));
%                 dyidxir(ixr) = interpTable(tab(:,1), tab(:,3), xi(ixr));
%             else
                dyidxil(ixl) = dinterpTable(tab(:,1), tab(:,2), xi(ixl));
                dyidxir(ixr) = dinterpTable(tab(:,1), tab(:,2), xi(ixr));
%             end
        end
    end
end
yi = yil.*w + yir.*(1-w);
if compDer
    dyidxi = dyidxil.*w + dyidxir.*(1-w);
    dyidvi = (yil-yir).*dwdvi;
end
end
