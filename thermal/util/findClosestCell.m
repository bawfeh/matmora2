function Ind = findClosestCell(g,x,y,z,sub)
% Find the cell with center closest to the point (x,y)

if nargin < 5
    sub = 1:g.cells.num;
end

xc = g.cells.centroids(sub,1);
yc = g.cells.centroids(sub,2);
zc = g.cells.centroids(sub,3);


Ind = zeros(numel(x),1);
for i = 1:numel(x)
    d = (xc - x(i)).^2 + (yc - y(i)).^2;
    
    if ~isempty(z)
        d = d + (zc - z(i)).^2;
    end
    d = sqrt(d);
    
    [~,ind] = min(d);
    if nargin == 5 
        if numel(sub)==g.cells.num
            sub = find(sub);
        end
        ind = sub(ind);
    end
    Ind(i) = ind;
end