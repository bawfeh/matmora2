function mu = co2viscosity(p,T,f)
a = [0.235156, -0.491266, 5.211155e-2, 5.347906e-2, -1.537102e-2];
b = [0.4071119e-2, 0.7198037e-4, 0.2411697e-16, 0.2971072e-22, -0.1627888e-22];
ek = 251.196;

lnTs = log(T/ek);
Ts = T./ek;
eta0 = a(1) + a(2) * lnTs + a(3) * power(lnTs,2.0) ...
                     + a(4) * power(lnTs,3.0) + a(5) * power(lnTs,4.0);
eta1 = exp(eta0);
eta0 = 1.00697*power(T,0.5)./eta1;
rho = f.bG(p,T)*f.rhoGS;
delta = b(1)*rho + b(2)*power(rho,2.0) ...
                 + b(3)*power(rho,6.0)./power(Ts,3.0) ...
                 + b(4)*power(rho,8.0) ...
                 + b(5)*power(rho,8.0)./Ts;
                 
mu = (eta0 + delta) * 1e-6;  %% convert into [Pas]

 end