function density = cubicEquationOfStateADI(p,T,eos,varargin)


assert(all(size(p)==size(T)), 'input matrices/vectors must be of the same dimension!')

% R = 8.3145;  % universal specific molar gas constant
R = 8.3144621;
d = eos.components.d; % mixing parameters
N = eos.components.ratios; % molar ratios
M = eos.components.Mass; % molar masses
Tc = eos.components.Tc; % critical temperatures
pc = eos.components.pc; % critical temperatures

eosparms = eos.parameters; % parameters of cubic equation of state

if (nargin>3)
    moles = varargin{1};
else
    moles = 1;
end

Z = solve(p,T,N,pc,Tc,R,d,eosparms);

kg = dot(N,M);

density = (kg*p)./(Z .* ((moles*R).*T)); 


%==========================

    function Z = solve(p,T,N,pc,Tc,R,d,eos)
        numc = numel(N);
        a0 = cell(1,numc);
        for nu = 1:numc
            Tr  = T./Tc;
            alpha = 1+ eos.kappa .* (1 - Tr.^0.5 );
            a0{nu} = eos.omegaA .* ((R*Tc(nu)) .* alpha).^2 ./ pc(nu);
        end
        
        a = 0.*T; % initialize
        for i = 1:numc
            for j = 1:numc
                sqrta0a0 = (a0{i} .* a0{j}).^0.5;
                a1 = (1-d(i,j)) .* sqrta0a0;
                a = a + (N(i)*N(j)) .*  a1;
            end
        end
        b0 = eos.omegaB * R  * (Tc./pc);
        b  = dot(N,b0);

        RT = R.*T;
        A = (a.*p) ./ (RT.^2);
        B = (b.*p) ./ RT; 
        
       Z = computeZfactor(A,B,eos.delta1,eos.delta2);
        
    end

      function  Z  = computeZfactor(A,B,delta1,delta2)

          a0 = -A .* B  -  (delta1 * delta2).*((B.^2) .* (1 + B)) ;
          a1 = A + (delta1 + delta2).*(B .* (1 + B)) + (delta1 * delta2) .* (B.^2);
          a2 = -(1 + delta1 + delta2).* B -  1;
          
%           [zMin,zMid,zMax] = CubicEquation(a0, a1, a2);
          % choose right root
%           Z = zMax;
            Z = CubicEquation(a0, a1, a2);
          assert(all(Z>0),'non-positive compressibility factor')
      end

    function [zMax,zMid,zMin] = CubicEquation(a0, a1, a2)
        Q = (3 * a1 - a2.^2) / 9.;
        Rs = (9 * a1 .* a2 - 27 * a0 - 2 * a2.^3) / 54.;
        D = Q.^3 + Rs.^2;
%         % One real root (D > 0 | Q == 0)
            Sarg = Rs + D.^0.5;
            Targ = Rs - D.^0.5;
            S = sign(Sarg) .* (abs(Sarg)).^(1. / 3.);
            Ts = sign(Targ) .* (abs(Targ)).^(1. / 3.);
            z0 = -a2 / 3. + S + Ts;
            zMin = z0;
            zMid = z0;
            zMax = z0;
%             zMin(D <= 0 ) = inf; 
%             zMid(D <= 0 ) = inf;
%             zMax(D <= 0 ) = inf;
%             zMin(D <= 0 ) = inf; 
%             zMid(D <= 0 ) = inf;
%             zMax(D <= 0 ) = inf;
            D(Q<=0 & Q>=0) = 1;
            assert(all(D>0), 'multiple cubic roots for eos!')  
%         end
% %         if (~all(only(:))) % Three real roots
% %             disp('multiple roots for cubic eos')
%             theta = acos(Rs ./ ((-Q.^3).^0.5));
%             factor = 2 * (-Q).^0.5;
%             z1 = factor .* cos(theta / 3.) - a2/3;
%             z2 = factor .* cos((theta + 2 *pi) / 3.) - a2/3;
%             z3 = factor .* cos((theta + 4 * pi) / 3.) - a2/3;
%             z = sort([z1,z2,z3],2);
%             zMin(D <= 0 & Q ~= 0) = z(:,1);
%             zMid(D <= 0 & Q ~= 0) = z(:,2);
%             zMax(D <= 0 & Q ~= 0) = z(:,3);
% %         end
    end


end

