function fluid = co2brineFromDecks(fluid,path)

temps = [300,310,320,330];
fluids = cell(numel(temps),1);
PVTO{1}.data = [];
PVTO{1}.key = temps';
PVTO{1}.pos = 1;
for i = 1: numel(temps)
    fn    = fullfile(path,['blackoil_pvt_dg_',int2str(temps(i))]);
    [fid, msg] = fopen(fn, 'rt');
    deck = initializeDeck;
    dirname = fileparts(fn);
    deck = readPROPS(fid, dirname, deck);
    deck.RUNSPEC.METRIC = true;
    deck = convertDeckUnits(deck);
    fluids{i} = initDeckADIFluid(deck);
    bO{i}.data = [deck.PROPS.PVTO{1}.data(:,1),1./deck.PROPS.PVTO{1}.data(:,2)];
    bO{i}.pos = deck.PROPS.PVTO{1}.pos;
    bO{i}.key = deck.PROPS.PVTO{1}.key;
    
    muO{i}.data = [deck.PROPS.PVTO{1}.data(:,1),deck.PROPS.PVTO{1}.data(:,3)];
    muO{i}.pos = deck.PROPS.PVTO{1}.pos;
    muO{i}.key = deck.PROPS.PVTO{1}.key;
    
    bG{i} = [deck.PROPS.PVDG{1}(:,1),1./deck.PROPS.PVDG{1}(:,2)];
    muG{i} = [deck.PROPS.PVDG{1}(:,1),deck.PROPS.PVDG{1}(:,3)];
    
end

fluid.fluids = fluids;
%x1 = 5*barsa;
%x2 = 650;
%x3 = 300;
%x1 = ones(3,1)*x1;
%x2 = ones(3,1)*x2;
%x3 = (300:5:310)';
%isSat = false(3,1);
%[y, dydx1, dydx2, dydvi] = interp3DPVT(Tab, x1, x2, isSat, x3);
Tab.data = bO;
Tab.key = temps';
fluid.bO = @(p,rs,flag,T) interp3DPVT(Tab,p,rs,flag,T);

Tab.data = muO;
fluid.muO = @(p,rs,flag,T) interp3DPVT(Tab,p,rs,flag,T);

Tab.data = vertcat(bG{:});
cs = cumsum(cellfun(@numel,bG,'UniformOutput',true)/2)';
Tab.pos = [1;cs(1:end)+1];
fluid.bG = @(p,T) interp2DPVT({Tab},p,T,{':'});

Tab.data = vertcat(muG{:});
fluid.muG = @(p,T) interp2DPVT({Tab},p,T,{':'});

tmp =  cellfun(@(x)[x.data(x.pos(1:end-1),1) x.key], bO, 'UniformOutput', false);
Tab.data = vertcat(tmp{:});
cs = cumsum(cellfun(@numel,tmp,'UniformOutput',true)/2)';
Tab.pos = [1;cs(1:end)+1];
fluid.rsSat = @(p,T) interp2DPVT({Tab},p,T,{':'});
