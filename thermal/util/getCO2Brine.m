function fluid = getCO2Brine(varargin)

%% add temperature in fluid
opt = struct('pvt', 'simple', ...
             'fluidprop', 'simple' ,... %
             'rho', [1000 1000 2], ...
             'cW' , 4.1813*10.^6/1e3, ...l % energy density per mass
             'cR' ,2.17*10.^6, ...% energy density per volume
             'Pe' ,10^4 * Pascal, ... % Entry pressure
             'lamda', 2, ... % lamda in corey brooks
             'n',[1 1 1], ... % only if relperm = simple
             'sr' , [0.2, 0.05 ],... % Residual saturation [wetting, nonwetting]
             'salinity', 0.10, ...  % brine salinity
             'eos', []);   % default parameters for cubic EOS
         
opt = merge_options(opt, varargin{:});

switch opt.fluidprop
    case 'simple'
            fluid_s = initSimpleADIFluid('n', opt.n);
            fluid.relPerm = fluid_s.relPerm;
    case 'corey-brooks'
        [krw,krnw,Pcnw] = CoreyBrooks(opt.Pe,opt.sr,opt.lamda);
        fluid.relPerm = @(sw,sg) CB2Relperm(krw,krnw,sw,sg);
        fluid.pcOG = @(so) Pcnw(so);
    case 'corey'
        [krw,krnw] = Corey(opt.sr,opt.n);
        fluid.relPerm = @(sw,sg) CB2Relperm(krw,krnw,sw,sg);
end

%density
fluid.rhoWS = opt.rho(1);
fluid.rhoOS = opt.rho(2);
fluid.rhoGS = opt.rho(3);

switch opt.pvt
    case 'simple'  %Span-Wagner table for pure CO2
        
%         data = []; P = []; T = [];
%         load('~/Documents/MATLAB/EOS_CO2/co2dataSW96.mat','P','T','data');  % bawfeh
%         [nT,nP] = size(data(:,:,1));
%         rho = data(2:nT,:,1)';
%         hC = data(2:nT,:,3)'*kilo;
%         ndata = numel(rho);
%         clear data
%         Tab.key  = T(2:nT); 
%         nT = numel(Tab.key);
%         Tab.pos = 1:nP:(ndata+1);
%         Tab.data = [repmat(P*mega,nT,1), reshape(rho,ndata,1)];
%         fluid.bG = @(p,T) interp2DPVT({Tab},p,T,{':'})/fluid.rhoGS;
%         
%         Tab.data = [repmat(P*mega,nT,1), reshape(hC,ndata,1)];
%         fluid.hG = @(p,T) interp2DPVT({Tab},p,T,{':'});
        
        tett = []; trykk = []; tempe = [];
        load('~/Documents/MATLAB/EOS_CO2/tetthet_SW_15_350.mat','tett','trykk','tempe')  % bawfeh
        j1 = find(tempe>300,1,'first');
        j2 = find(tempe<309,1,'last');
        i1 = find(trykk>4.9,1,'first');
        i2 = find(trykk<12,1,'last'); 
        T = tempe(j1:j2)';
        P = trykk(i1:i2)'*mega;
        data = tett(i1:i2,j1:j2);
        clear tempe trykk tett
        nT = numel((j1:j2));
        nP = numel((i1:i2));
        Tab.key  = T;
        Tab.pos = 1:nP:(nP*nT+1);
        Tab.data = [repmat(P,nT,1), reshape(data,nT*nP,1)];
        fluid.bG = @(p,T) interp2DPVT({Tab},p,T,{':'})/fluid.rhoGS;
        
        h = []; trykk = []; tempe = [];
        load('~/Documents/MATLAB/EOS_CO2/entalpi_SW_15_350.mat','h','trykk','tempe')  % bawfeh
        T = tempe(j1:j2)';
        P = trykk(i1:i2)';
        data = h(i1:i2,j1:j2);
        clear tempe trykk h
        Tab.key  = T;
        Tab.pos = 1:nP:(nP*nT+1);
        Tab.data = [repmat(P*mega,nT,1), reshape(data*mega,nT*nP,1)];
        fluid.hG = @(p,T) interp2DPVT({Tab},p,T,{':'});
        
         fluid.muG = @(p,T) co2viscosity(p,T,fluid);  %% following Vesovic et al.
        
        data = []; P = []; T = [];
        load('~/Documents/MATLAB/EOS_CO2/water_pvt_IAPWS95.mat','data','P','T')  % bawfeh
        nT = numel(T);
        nP = numel(P);
        hH2O = data(:,:,3)' * kilo;
        clear data;
        Tab.key = T;
        Tab.pos = 1:nP:(nP*nT+1);
        Tab.data = [repmat(P*mega,nT,1), reshape(hH2O,nT*nP,1)];
        fluid.hO = @(p,T) interp2DPVT({Tab},p,T,{':'});
        
        
% %         brine = brineProps(opt.salinity);
%         co2 = co2Props_simple; 
% %         fluid.bG = @(p,T) co2.density(p,T)/fluid.rhoGS;
% %         fluid.hG = @(p,T) co2.enthalpy(p,T);
%         fluid.muG = @(p,T) co2.viscosity(p,T);
%         
% %         fluid.hO = @(p,T) brine.enthalpy(p,T);
% %         fluid.muO = @(p,T) brine.viscosity(p,T);
% %         fluid.bO = @(p,T) brine.density(p,T)/fluid.rhoOS; 
        

    
    case 'cubic'
        opt.eos = updateEOS(opt.eos);
%         
%         P = (4.9:7.5e-3:12)'*mega;
%         T = (300:0.04:309)';
%         
% %         P = []; T = []; rho = [];
% %          load('~/Documents/MATLAB/EOS_CO2/PRdata.mat','rho','P','T')  % bawfeh
%         
%         nP = numel(P); nT = numel(T);
%         [XT,XP] = meshgrid(T,P);
%         data = cubicEquationOfState(XP,XT,opt.eos);
% %         data = rho; clear rho; 
%         clear XT YT
%         Tab.key  = T;
%         Tab.pos = 1:nP:(nP*nT+1);
%         Tab.data = [repmat(P,nT,1), reshape(data,nT*nP,1)];
%         fluid.bG = @(p,T) interp2DPVT({Tab},p,T,{':'})/fluid.rhoGS;
        fluid.bG = @(p,T) cubicEquationOfStateADI(p,T,opt.eos)/fluid.rhoGS;
        
        tempe = []; h = []; trykk = [];
        load('~/Documents/MATLAB/EOS_CO2/entalpi_SW_15_350.mat','h','trykk','tempe')  % bawfeh
        j1 = find(tempe>300,1,'first');
        j2 = find(tempe<309,1,'last');
        i1 = find(trykk>4.9,1,'first');
        i2 = find(trykk<12,1,'last'); 
        T = tempe(j1:j2)';
        P = trykk(i1:i2)'*mega;
        nT = numel(T); nP = numel(P);
        data = h(i1:i2,j1:j2)*mega;
        clear tempe trykk h
        Tab.key  = T;
        Tab.pos = 1:nP:(nP*nT+1);
        Tab.data = [repmat(P,nT,1), reshape(data,nT*nP,1)];
        fluid.hG = @(p,T) interp2DPVT({Tab},p,T,{':'});
        
        fluid.muG = @(p,T) co2viscosity(p,T,fluid);  %% following Vesovic et al.
        
        data = []; P = []; T = [];
        load('~/Documents/MATLAB/EOS_CO2/water_pvt_IAPWS95.mat','data','P','T')  % bawfeh
        nT = numel(T);
        nP = numel(P);
        hH2O = data(:,:,3)' * kilo;
        clear data
        Tab.key = T;
        Tab.pos = 1:nP:(nP*nT+1);
        Tab.data = [repmat(P*mega,nT,1), reshape(hH2O,nT*nP,1)];
        fluid.hO = @(p,T) interp2DPVT({Tab},p,T,{':'});
        
% %         brine = brineProps(opt.salinity);
%         co2 = co2Props_simple; 
% %         fluid.bG = @(p,T) co2.density(p,T)/fluid.rhoGS;
% %         fluid.hG = @(p,T) co2.enthalpy(p,T);
%         fluid.muG = @(p,T) co2.viscosity(p,T);
%         
% %         fluid.hO = @(p,T) brine.enthalpy(p,T);
% %         fluid.muO = @(p,T) brine.viscosity(p,T);
% %         fluid.bO = @(p,T) brine.density(p,T)/fluid.rhoOS; 

    case 'linear-table'        
        
        fluid_full = co2brineFromDecks([],fullfile(ROOTDIR,'../../pvt_co2/'));
        
        fluid_300 = fluid_full.fluids{1};
        fluid_330 = fluid_full.fluids{4};
        
        % assume linear function w.r.t T; 
        a_bO = @(p,rs,flag) (fluid_330.bO(p,rs,flag)-fluid_300.bO(p,rs,flag))/30;
        a_muO = @(p,rs,flag) (fluid_330.muO(p,rs,flag)-fluid_300.muO(p,rs,flag))/30;
        a_bG = @(p) (fluid_330.bG(p)-fluid_300.bG(p))/30;
        a_muG = @(p) (fluid_330.muG(p)-fluid_300.muG(p))/30;
        a_rsSat = @(p) (fluid_330.rsSat(p)-fluid_300.rsSat(p))/30;
        
        fluid.rsSat = @(p,T) fluid_300.rsSat(p)  + a_rsSat(p).*(T-300);

        fluid.muO = @(p,rs,flag,T) fluid_300.muO(p,rs,flag) + a_muO(p,rs,flag).*(T-300);
        fluid.bO = @(p,rs,flag,T) fluid_300.bO(p,rs,flag) + a_bO(p,rs,flag).*(T-300);

        fluid.muG = @(p,T) fluid_300.muG(p) + a_muG(p).*(T-300);
        fluid.bG = @(p,T) fluid_300.bG(p) + a_bG(p).*(T-300);
        
        fluid.uO = @(p,T) opt.cW.*T;
        fluid.uG = @(p,T) 0.1*opt.cW.*T;
        
        fluid.hG = @(p,T) 0.1*opt.cW.*T;
        fluid.hO = @(p,T) opt.cW.*T;
        
    case 'oil'
        fluid=initSimpleADIFluid('mu', [0.4 4 0.04]*centi*poise, 'rho', [1000 700 100], 'n', [2 2 2]);
        fluid.muW =@(p,T) fluid.muW(p)./(1+1e-2.*(T-300));
        fluid.muO =@(p,rs,isSat,T) fluid.muO(p,rs,isSat)./(1+10e-1.*(T-300));

        fluid.uO = @(p,T) opt.cW.*T;
        fluid.uG = @(p,T) 0.1*opt.cW.*T;
        
        fluid.hG = @(p,T) 0.1*opt.cW.*T;
        fluid.hO = @(p,T) opt.cW.*T;
    case 'table'
        
        fluid = co2brineFromDecks(fluid,fullfile(ROOTDIR,'../../pvt_co2/'));
        
        fluid.hO = @(p,rs,isSat,T) (T - 298.15)*5e3;
        fluid.hG = @(p,T) 571.3e3 + (T - 298.15)*0.85e3;
        % T0 = 25C and p0 = 1e7
        %fluid.hO = @(p,rs,isSat,T) 114e3 + (T - 298.15)*4.15e3;
        %fluid.hG = @(p,T) 257.15e3 + (T - 298.15)*2.9415e3;
       
    case 'mix'
        fluid = co2brineFromDecks(fluid,fullfile(ROOTDIR,'../../pvt_co2/'));
        co2 = co2Props_sol('density_sal=10','entalpy_sal=10','solubility_sal=10');
        brine = brineProps(opt.salinity); 
        %fluid.muO = @(p,rs,isSat,T) brine.viscosity(p,T);
        %fluid.muG = @(p,T) co2.viscosity(p,T);
        
        fluid.hG = @(p,T) co2.entalpy(p,T);
        fluid.hO = @(p,rs,isSat,T) brine.enthalpy(p,T);
        
    case 'spanwagner'
       co2 = co2Props_sol('density_sal=15','entalpy_sal=15','solubility_sal=15');
       brine = brineProps(opt.salinity); 
       co2.mM = 44e-3; %kilogram/mol;
       h2o.mM = 18e-3; %kilogram/mol;
       NaCl.mM = 58e-3;
       brine.mM = (h2o.mM * NaCl.mM) / (NaCl.mM + opt.salinity*( h2o.mM - NaCl.mM));
       
       mRhoCo2 = fluid.rhoGS./co2.mM;
       mRhoBrine = fluid.rhoOS./brine.mM;
             
       molefrac = @(p,T) co2.sol(p,T).*brine.mM ./ ( co2.sol(p,T).*brine.mM + (1-co2.sol(p,T))*co2.mM);
       %molefrac = @(p,T) co2.sol(p,T);
       
       fluid.bG = @(p,T) co2.density(p,T)./fluid.rhoGS;
       fluid.hG = @(p,T) co2.entalpy(p,T);
       fluid.muG = @(p,T) co2.viscosity(p,T);
      
       %rs2x = @(p,rs,isSat,T) (1-isSat).*(mRhoCo2*rs) ./ (rs * mRhoCo2 + mRhoBrine) + isSat .*molefrac(p,T);  
       
       %rs2w = @(p,rs,isSat,T) rs2x(p,rs,isSat,T) * co2.mM ./ ( (1-rs2x(p,rs,isSat,T))*brine.mM + rs2x(p,rs,isSat,T) * co2.mM);
       
       rs2w = @(p,rs,isSat,T) (rs*fluid.rhoGS)./(fluid.rhoOS+rs*fluid.rhoGS);
       
       %fluid.hO = @(p,rs,isSat,T) brine.enthalpy(p,T) + rs2w(p,rs,isSat,T).*(fluid.hG(p,T) - brine.enthalpy(p,T));
       fluid.muO = @(p,rs,isSat,T) brine.viscosity(p,T);
       fluid.bO = @(p,rs,isSat,T) (brine.density(p,T).*( 1 + rs2w(p,rs,isSat,T)))./fluid.rhoOS;
       %fluid.bO = @(p,rs, isSat,T) brine.density(p,T)./fluid.rhoOS;
       
       
       %fluid.rsSat = @(p,T) molefrac(p,T);
       fluid.rsSat = @(p,T) fluid.rhoOS*(co2.sol(p,T)) ./ (fluid.rhoGS*(1-co2.sol(p,T))); 
       fluid.rsSat2 = @(p,T) mRhoBrine.*(molefrac(p,T)) ./ (mRhoCo2.*(1-molefrac(p,T)) ) ;
    case 'spanwagner_nomix'
       co2 = co2Props_sol('density_sal=1','entalpy_sal=1','solubility_sal=1');
       brine = brineProps(opt.salinity); 
       co2.mM = 44e-3; %kilogram/mol;
       h2o.mM = 18e-3; %kilogram/mol;
       NaCl.mM = 58e-3;
       brine.mM = (h2o.mM * NaCl.mM) / (NaCl.mM + opt.salinity*( h2o.mM - NaCl.mM));
       
       fluid.bG = @(p,T) co2.density(p,T)./fluid.rhoGS;
       %fluid.hG = @(p,T) co2.entalpy(p,T);
       fluid.muG = @(p,T) co2.viscosity(p,T);
      
       %fluid.rs2x = @(p,rs,isSat,T) (1-isSat).*(fluid.rhoGS*rs) ./ (rs * fluid.rhoGS + fluid.rhoOS) + isSat .*co2.sol(p,T);  
       
       %fluid.hO = @(p,rs,isSat,T) brine.enthalpy(p,T) + fluid.rs2x(p,rs,isSat,T).*(fluid.hG(p,T) - brine.enthalpy(p,T));
       %fluid.muO = @(p,rs,isSat,T) brine.viscosity(p,T);
       fluid.muO = @(p,T) brine.viscosity(p,T);
       %fluid.bO = @(p,rs,isSat,T) (brine.density(p,T).*( 1 - fluid.rs2x(p,rs,isSat,T)))./fluid.rhoOS;
       fluid.bO = @(p,T) brine.density(p,T)./fluid.rhoOS;
       
       %fluid.rsSat = @(p,T) fluid.rhoOS*co2.sol(p,T) ./ (fluid.rhoGS*(1-co2.sol(p,T))); 
       % fluid.rsSat = @(p,T) brine.mM.*fluid.rhoOS*co2.sol(p,T) ./ (co2.mM.*fluid.rhoGS*(1-co2.sol(p,T)));
               
    otherwise
        error();
end
% water. not used 
fluid.uW = @(p,T) opt.cW.*T;
fluid.hW = @(p,T) opt.cW.*T;     
fluid.bW = @(p,T) p*0 + 1;
fluid.muW = @(p,T) p*0 + 0.4 * centi*poise;
    
%fluid.hO = @(p,rs,isSat,T) opt.cW.*T;
%fluid.hG = @(p,T) 0.7 * opt.cW.*T;

fluid.uR = @(T) opt.cR.*T;
