function eos = updateEOS(eos)

if (isfield(eos,'parameters'))
    assert(isfield(eos.parameters,'delta1'), 'cubic eos "parameter" field lacks the ''delta1'' subfield')
    assert(isfield(eos.parameters,'delta2'), 'cubic eos "parameter" field lacks the ''delta2'' subfield')
    assert(isfield(eos.parameters,'kappa'), 'cubic eos "parameter" field lacks the ''kappa'' subfield')
    assert(isfield(eos.parameters,'omegaA'), 'cubic eos "parameter" field lacks the ''omegaA'' subfield')
    assert(isfield(eos.parameters,'omegaB'), 'cubic eos "parameter" field lacks the ''omegaB'' subfield')
else
    eos.parameters = struct('delta1',-1-sqrt(2),'delta2',-1+sqrt(2),'kappa',0.7056,...
    'omegaA',0.4572,'omegaB',0.0778);  %% defaults to Peng-Robinson parameters
end
            
if (isfield(eos,'components'))
    assert(isfield(eos.components,'d'),...
        'cubic eos "components" field lacks subfield ''d'' for mixing parameters')
    assert(isfield(eos.components,'ratios'),...
        'cubic eos "components" field lacks subfield ''ratios'' for mole ratios')
    assert(isfield(eos.components,'Mass'),...
        'cubic eos "components" field lacks subfield ''Mass'' for molar masses')
    assert(isfield(eos.components,'Tc'),...
        'cubic eos "components" field lacks subfield ''Tc'' for critical absolute temperatures')
    assert(isfield(eos.components,'pc'),...
        'cubic eos "components" field lacks subfield ''pc'' for critical pressures')
    assert(isfield(eos.components,'names'),...
        'cubic eos "components" field lacks subfield ''names'' - array of strings')
    numComponents = numel(eos.components.names);
    assert(numel(eos.components.ratios)==numComponents,...
        'eos.components.ratios must be a vector of dimension [%dx1]', numComponents)
    assert(numel(eos.components.Mass)==numComponents,...
        'eos.components.Mass must be a vector of dimension [%dx1]', numComponents)
    assert(numel(eos.components.Tc)==numComponents,...
        'eos.components.Tc must be a vector of dimension [%dx1]', numComponents)
    assert(numel(eos.components.pc)==numComponents,...
        'eos.components.pc must be a vector of dimension [%dx1]', numComponents)
    assert(all(size(eos.components.d) == numComponents),...
        'eos.components.d must be a matrix of dimension [%dx%d]', numComponents, numComponents)
    assert(sum(eos.components.ratios)==1,'total compositions must sum up to 1!')
    assert(all(diag(eos.components.d)==0),'inconsistent mixing parameters')
    if isrow(eos.components.ratios), eos.components.ratios = eos.components.ratios'; end
    if isrow(eos.components.Mass), eos.components.Mass = eos.components.Mass'; end
    if isrow(eos.components.Tc), eos.components.Tc = eos.components.Tc'; end
    if isrow(eos.components.pc), eos.components.pc = eos.components.pc'; end
else
    % use default  Peng-Robinson, for pure CO2
    eos.components.names = {'CO2'}; % components
    eos.components.d = 0;  % mixing parameters
    eos.components.ratios = 1;  % molar ratios
    eos.components.Mass = 0.04401; % molar mass for CO2 [kg/mol]
    eos.components.Tc = (31.2+273.15); % critical absolute temperature for CO2 [K]
    eos.components.pc = 7.398e+6; % critical pressure for CO2 [Pa]
end

end