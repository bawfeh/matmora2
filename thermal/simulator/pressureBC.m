function  [bWqWbc,bOqObc,bGqGbc] = pressureBC(G,s,p,rhoW,rhoO,rhoG,mobW,mobO,mobG,bW,bO,bG,pcOW,pcOG,bc)        
        assert(all(strcmp(bc.type,'pressure')),'only pressure bc allowed');
        Tbc = s.T_all(bc.face);
        assert(all(sum(G.faces.neighbors(bc.face,:)>0,2)==1),'bc on internal boundary');
        bc_cell = sum(G.faces.neighbors(bc.face,:),2);
        
        dzbc = (G.cells.centroids(bc_cell,3)-G.faces.centroids(bc.face,3));
        g = norm(gravity());
        c2f = bc.cell2bcface;
        pObc = c2f*p;
        rhoObc=c2f*rhoO;
        dpbc_o = bc.value-pObc+g*(rhoObc.*dzbc);
        %pWbc= pObc-pcOW(bc_cell);rhoWbc=rhoW(bc_cell);
        
        pWbc = pObc;
        if numel(pcOW)==G.cells.num
            pWbc = pWbc-pcOW(bc_cell);
        end
        
        rhoWbc=c2f*rhoW;% neglect capillary pressure
        dpbc_w=bc.value-(pWbc)+g*(rhoWbc.*dzbc);
        
        pGbc = pObc;
        if numel(pcOW)==G.cells.num
            pGbc = pGbc+pcOG(bc_cell);
        end

        rhoGbc=c2f*rhoG;% neglect capillary pressure
        dpbc_g=bc.value-(pGbc)+g*(rhoGbc.*dzbc);
        
        
        bWmobWbc = (c2f*bW).*(c2f*mobW);
        %bOmobObc = (c2f*(bO.*mobO));
        bOmobObc = (c2f*bO).*(c2f*mobO);% seems to be faster
        
        bGmobGbc = (c2f*bG).*(c2f*mobG);
        
        %bWmobWbc(dpbc_o>0)=0;
        %if(any(dpbc_w>0))
        %    bWmobWbc(dpbc_w>0)=bW(bc_cell(dpbc_w>0)).*(mobW(bc_cell(dpbc_w>0))+mobO(bc_cell(dpbc_w>0)));     
        %end
        bWqWbc  = -bWmobWbc.*Tbc.*(dpbc_w);
        bOqObc  = -bOmobObc.*Tbc.*(dpbc_o);
        bGqGbc  = -bGmobGbc.*Tbc.*(dpbc_g);
        % convert to full variables
        %bWqWbc = bca.bcface2cell*bWqWbc;
        %bOqObc = bca.bcface2cell*bOqObc;
end