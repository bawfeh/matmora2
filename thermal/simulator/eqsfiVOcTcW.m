function [eqs, state] = eqsfiVOcTcW(state0, state, dt, G, W, s, f, system, varargin)
% Generate equations for a Volatile 3Ph system (wet-gas, live-oil).
    opt = struct('Verbose',     mrstVerbose,...
                 'reverseMode', false,...
                 'scaling',     [],...
                 'resOnly',     false,...
                 'history',     [],  ...
                 'iteration',   -1,  ...
                 'stepOptions', []);

    opt = merge_options(opt, varargin{:});

    disgas = system.activeComponents.disgas;
    vapoil = system.activeComponents.vapoil;

    % current variables: ------------------------------------------------------
    p    = state.pressure;
    sW   = state.s(:,1);
    sG   = state.s(:,3);
    rs   = state.rs;
    rv   = state.rv;
    T   = state.T;

    bhp = vertcat(state.wellSol.bhp);
    qWs    = vertcat(state.wellSol.qWs);
    qOs    = vertcat(state.wellSol.qOs);
    qGs    = vertcat(state.wellSol.qGs);

    % previous time-step variables ------------------------------------------------------
    p0  = state0.pressure;
    sW0 = state0.s(:,1);
    sG0 = state0.s(:,3);
    rs0 = state0.rs;
    rv0 = state0.rv;
    T0 = state0.T;

    %Initialization of primary variables ----------------------------------
    [st1 , st2  , st3 ] = getCellStatus(state , disgas, vapoil);
    [st1p, st2p , st3p] = getCellStatus(state0, disgas, vapoil);
    if ~opt.resOnly,
        if ~opt.reverseMode,
            % define primary varible x and initialize
            x = st1.*rs + st2.*rv + st3.*sG;

            [p, x, qOs, qGs, bhp] = ...
                initVariablesADI(p, x, qOs, qGs, bhp);
            % define sG, rs and rv in terms of x
            sG = st2.*(1-sW) + st3.*x;
            if disgas
                rsSat = f.rsSat(p,T);
                rs = (~st1).*rsSat + st1.*x;
            else % otherwise rs = rsSat = const
                rsSat = rs;
            end
            if vapoil
                rvSat = f.rvSat(p,T);
                rv = (~st2).*rvSat + st2.*x;
            else % otherwise rv = rvSat = const
                rvSat = rv;
            end
        else
            x0 = st1p.*rs0 + st2p.*rv0 + st3p.*sG0;

            [p0, sW0, x0, zw, zw, zw, zw] = ...
                initVariablesADI(p0, sW0, x0, ...
                zeros(size(qWs)) , zeros(size(qOs)) , ...
                zeros(size(qGs)) , zeros(size(bhp)));                 %#ok
            sG0 = st2p.*(1-sW0) + st3p.*x0;
            if disgas
                rsSat0 = f.rsSat(p0);
                rs0 = (~st1p).*rsSat0  + st1p.*x0;
            else 
                rsSat0 = rs0; % Not used - remove
            end
            if vapoil
                rvSat0 = f.rvSat(p0);
                rv0 = (~st2p).*rvSat0  + st2p.*x0;
            else
                rvSat0 = rv0; % Not used - remove
            end
        end
    else % resOnly-case compute rsSat and rvSat for use in well eqs
        if disgas, rsSat = f.rsSat(p); else rsSat = rs; end
        if vapoil, rvSat = f.rvSat(p); else rvSat = rv; end
    end
    %----------------------------------------------------------------------
    %check for p-dependent tran mult:
    trMult = 1;
    if isfield(f, 'tranMultR'), trMult = f.tranMultR(p); end

    %check for p-dependent porv mult:
    pvMult = 1; pvMult0 = 1;
    if isfield(f, 'pvMultR')
        pvMult =  f.pvMultR(p);
        pvMult0 = f.pvMultR(p0);
    end

    %check for capillary pressure (p_cow)
    pcOW = 0;
    if isfield(f, 'pcOW')
        pcOW  = f.pcOW(sW);
    end
    %check for capillary pressure (p_cog)
    pcOG = 0;
    if isfield(f, 'pcOG')
        pcOG  = f.pcOG(1-sW-sG);
    end

    % FLIUD PROPERTIES ---------------------------------------------------
    [krW, krO, krG] = f.relPerm(sW, sG);
    g  = norm(gravity);
    dz = s.grad(G.cells.centroids(:,3));

    % WATER PROPS (calculated at oil pressure)
    bW     = f.bW(p);
    rhoW   = bW.*f.rhoWS;
    % rhoW on face, avarge of neighboring cells (E100, not E300)
    rhoWf  = s.faceAvg(rhoW);
    mobW   = trMult.*krW./f.muW(p,T);
    dpW    = s.grad(p-pcOW) - g*(rhoWf.*dz);
    % water upstream-index
    upc  = (double(dpW)>=0);
    bWvW = s.faceUpstr(upc, bW.*mobW).*s.T.*dpW;

    % OIL PROPS
    if disgas
        bO  = f.bO(p, rs, ~st1,T);
        muO = f.muO(p, rs, ~st1,T);
    else
        bO  = f.bO(p,T);
        muO = f.muO(p,T);
    end
    rhoO   = bO.*(rs*f.rhoGS + f.rhoOS);
    rhoOf  = s.faceAvg(rhoO);
    mobO   = trMult.*krO./muO;
    dpO    = s.grad(p) - g*(rhoOf.*dz);
    % oil upstream-index
    upc = (double(dpO)>=0);
    bOvO   = s.faceUpstr(upc, bO.*mobO).*s.T.*dpO;
    if disgas, rsbOvO = s.faceUpstr(upc, rs).*bOvO;end

    % GAS PROPS (calculated at oil pressure)
    if vapoil
        bG  = f.bG(p, rv, ~st2,T);
        muG = f.muG(p, rv, ~st2,T);
    else
        bG  = f.bG(p,T);
        muG = f.muG(p,T);
    end
    rhoG   = bG.*(rv*f.rhoOS + f.rhoGS);
    rhoGf  = s.faceAvg(rhoG);
    mobG   = trMult.*krG./muG;
    dpG    = s.grad(p+pcOG) - g*(rhoGf.*dz);
    % gas upstream-index
    upc    = (double(dpG)>=0);
    bGvG   = s.faceUpstr(upc, bG.*mobG).*s.T.*dpG;
    if vapoil, rvbGvG = s.faceUpstr(upc, rv).*bGvG; end
    
     
    % BC
    if isfield(system,'bc')
        [bWqWbc,bOqObc,bGqGbc] = pressureBC(G,s,p,rhoW,rhoO,rhoG,mobW,mobO,mobG,bW,bO,bG,pcOW,pcOG,system.bc);
    end
    % EQUATIONS -----------------------------------------------------------
    sO  = 1- sW  - sG;
    sO0 = 1- sW0 - sG0;

    bW0 = f.bW(p0,T0);
    if disgas, bO0 = f.bO(p0, rs0, ~st1p,T0); else bO0 = f.bO(p0,T0); end
    if vapoil, bG0 = f.bG(p0, rv0, ~st2p,T0); else bG0 = f.bG(p0,T0); end

    if disgas, rhoO0 = bO0.*(rs0*f.rhoGS + f.rhoOS); else rhoO0 = bO0.*f.rhoOS; end
    if vapoil, rhoG0 = bG0.*(rv0*f.rhoOS + f.rhoGS); else rhoG0 = bG0.*f.rhoGS; end
    rhoW0 = bW0.*f.rhoWS;   
        
    bFsF = {bO.*sO,bG.*sG};
    if vapoil, bFsF{1} = bFsF{1} + rv.*bG.*sG; end
    if disgas, bFsF{2} = bFsF{2} + rs.*bO.*sO; end
    
    bFsF0={bO0.*sO0,bG0.*sG0};
    if vapoil, bFsF0{1} = bFsF0{1} + rv0.*bG0.*sG0; end
    if disgas, bFsF0{2} = bFsF0{2} + rs0.*bO0.*sO0; end
    
    bFvF={bOvO,bGvG};
    if vapoil, bFvF{1} = bFvF{1} + rvbGvG; end
    if disgas, bFvF{2} = bFvF{2} + rsbOvO; end    
  
    state.gF = bFvF{2}.val;
    
    
    for i = 1:2
        eqs{i} = (s.pv/dt).* (pvMult.*bFsF{i} - pvMult0.*bFsF0{i}) + ...
            s.div(bFvF{i});
    end
    
    % BC
    if isfield(system,'bc')
        eqs{1}  = eqs{1} + system.bc.bcface2cell*bOqObc;
        eqs{2}  = eqs{2} + system.bc.bcface2cell*bGqGbc;
    end
    %eqs{3}  = eqs{3} + bc.bcface2cell*bWqWbc;        

%     % Temperature eq:
%     rhoS={f.rhoOS,f.rhoWS, f.rhoGS};
% 
%     if disgas, hO = f.hO(p,rs,~st1,T); else hO = f.hO(p,T); end
%     if vapoil, hG = f.hG(p,rv,~st2,T); else hG = f.hG(p,T); end
%     
%     if disgas, hO0 = f.hO(p0,rs0,~st1p,T0); else hO0 = f.hO(p0,T0); end
%     if vapoil, hG0 = f.hG(p0,rv0,~st2p,T0); else hG0 = f.hG(p0,T0); end
%         
%     hF={hO, f.hW(p,T), hG};
%     
%     hF0={hO0, f.hW(p0,T0), hG0};
%     
%     eF={hF{1} - p./rhoO , hF{2} - p./rhoW, hF{3} - p./rhoG};
%     
%     eF0={hF0{1} - p0./rhoO0 , hF0{2} - p0./rhoW0, hF0{3} - p0./rhoG0};
%     
%     uR=f.uR(T);uR0=f.uR(T0);
%     vQ = s.T_r .* s.grad(T);
% 
%     eqs{4} = ((G.cells.volumes-s.pv)/dt).*(  (pvMult).*uR-(pvMult0).*uR0) + s.div( vQ);
%     for i=1:numel(eF)       
%         eqs{4}  =  eqs{4} + ((s.pv/dt).*( pvMult.*eF{i}.*rhoS{i}.*bFsF{i} - pvMult0.*eF0{i}.*rhoS{i}.*bFsF0{i} )...
%                 +  s.div( s.faceUpstr(bFvF{i}>0, rhoS{i}.*hF{i}) .* bFvF{i}));         
%     end
    
    
    % well equations
    if ~isempty(W)
        wc    = vertcat(W.cells);
        if ~opt.reverseMode
            nperf = numel(wc);
            pw    = p(wc);
            rhows = [f.rhoWS, f.rhoOS, f.rhoGS];
            bw    = {bW(wc), bO(wc), bG(wc)};
            if ~disgas
                rsw = ones(nperf,1)*rs; rsSatw = ones(nperf,1)*rsSat; %constants
            else
                rsw = rs(wc); rsSatw = rsSat(wc);
            end
            if ~vapoil
                rvw = ones(nperf,1)*rv; rvSatw = ones(nperf,1)*rvSat; %constants
            else
                rvw = rv(wc); rvSatw = rvSat(wc);
            end
            rw    = {rsw, rvw};
            rSatw = {rsSatw, rvSatw};
            mw    = {mobW(wc), mobO(wc), mobG(wc)};

            [well_eqs, cqs, state.wellSol] = getWellContributions(...
                W, state.wellSol, bhp, {qWs,qOs,qGs}, pw, rhows, bw, rw, rSatw, mw, ...
                'iteration', opt.iteration, ...
                'model', 'VO');
            eqs(3:5) = well_eqs(2:4);

            eqs{1}(wc) = eqs{1}(wc) - cqs{2}; % Add src to oil eq
            %eqs{2}(wc) = eqs{2}(wc) - cqs{1}; % Add src to water eq
            eqs{2}(wc) = eqs{2}(wc) - cqs{3}; % Add src to gas eq
        else
            % Force wells to be ADI variables.
            nw = numel(state0.wellSol);
            zw = double2ADI(zeros(nw,1), p0);
            eqs(3:5) = {zw, zw, zw, zw};
        end
    else
        eqs(3:5) = {bhp, bhp, bhp, bhp};  % empty  ADIs
    end
    
    % add effect of wells to temperature eq
    %bFqF={bq{2},bq{1},bq{3}};
    %cbFqF = bFqF;
    %cbFqF{3} = cbFqF{3} + rs(wc).*bq{2};
%     cbFqF = {cqs{2},cqs{1},cqs{3}};
%     if disgas
%        bFqF={cqs{2},cqs{1},cqs{3} - rs(wc).*cqs{2}};
%     else
%         bFqF={cqs{2},cqs{1},cqs{3}};       
%     end
%        
%     
%     hFwp=cell(3,1);
%     chF = cell(3,1);
%     
%      for i=1:numel(W)
%          nperf = numel(W(i).cells);
%         if W(i).sign<0
%             for j = 1:3
%                 chF{j} = zeros(nperf,1);
%             end
%         else
%             cpress  = state.wellSol(i).cdp + state.wellSol(i).bhp;
%             ctemp = W(i).T * ones(nperf,1);
%             if disgas
%                 chF{1} = f.rhoOS*f.hO(cpress,zeros(nperf,1),false(nperf,1),ctemp);
%             else
%                 chF{1} = f.rhoOS*f.hO(cpress,ctemp);
%                 
%             end
%             chF{2} = f.rhoWS*f.hW(cpress,ctemp);
%             chF{3} = f.rhoGS*f.hG(cpress,ctemp);
% 
%         end       
%        hFwp{1}=[hFwp{1};chF{1}];
%        hFwp{2}=[hFwp{2};chF{2}];
%        hFwp{3}=[hFwp{3};chF{3}];
%     end
%     HqH=cell(3,1);
%     for i=1:3
%         HqH{i}  = rhoS{i}.*hF{i}(wc).*cbFqF{i};
%         ind=bFqF{i}>0;
%         HqH{i}(ind)  = hFwp{i}(ind).*bFqF{i}(ind);
%     end
%     %% add well contributioin
%     for i=1:3
%        eqs{4}(wc) = eqs{4}(wc)  -  HqH{i};
%     end
%     eqs{4}=eqs{4}/1e6;
    
    
end
%--------------------------------------------------------------------------
function [st1, st2, st3] = getCellStatus(state, disgas, vapoil)
% Status should be passed on from updateStateVO (to be sure definition is
% identical). rs and rv are assumed to be compatible, i.e. rx = rxSat for
% saturated cells and rx <= rxSat for undersaturated. Three values of
% status are:
% status 0: should not occur (almost water only -> state 3)
% status 1 oil, no gas  : x = rs, sg = 0    , rv = rvMax
% status 2 gas, no oil  : x = rv, sg = 1-sw , rs = rsMax
% status 3 oil and gas  : x = sg, rs = rsMax, rv = rvMax
if isfield(state, 'status')
    status = state.status;
else
    s = state.s;
    watOnly    = s(:,1) > 1- sqrt(eps);
    if ~vapoil
        oilPresent = true;
    else
        oilPresent = or(s(:,2) > 0, watOnly);
    end
    if ~disgas
        gasPresent = true;
    else
        gasPresent = or(s(:,3) > 0, watOnly);
    end
    status = oilPresent + 2*gasPresent;
end
if ~disgas
    st1 = false;
else
    st1 = status==1;
end
if ~vapoil
    st2 = false;
else
    st2 = status==2;
end
st3 = status == 3;
end

