%% Example by A. Bielinski (2006)

% mrstModule add ad-fi deckformat ad-props
% % workspace_path = '~/workspace/mrst-thermal/';
% % addpath(genpath([workspace_path,'thermal/util']))
% % addpath(genpath([workspace_path,'thermal/simulator']))
% addpath(genpath('~/Documents/MATLAB/thermal'))

%% INPUT
clc
clear

load('axes4');  % xx, yy, zz, depth, Ix, right, bottom, inj 

Iz = Ix + depth; Iy = yy'; % Physical dimensions of reservoir

mrstVerbose(1);
outputdir = '~/Documents/MATLAB/thermal/examples/outputT307axes4';
saveasdir = '~/Documents/Notes/ECMOR14/Bielinski06T307axes4days40';
format = 'fig';
format2 = '-pdf';
editflag = true;

perm    = 1.0 * darcy;            % Permeability 
phi        = 0.2;             % Porosity

nx = size(xx,1)-1;
ny = size(yy,1)-1;
nz = size(zz,1)-1;

largeEps = 1e-7;

gravity('on')

 restart = 0; 

%% The grid

G = tensorGrid(xx,yy,zz,'depthz',repmat(depth,nx+1,ny+1));
G = computeGeometry(G);
%%
% figure(1), clf
% plotGrid(G, 'FaceColor', 'yellow', 'EdgeAlpha', 0.5), view(-20,20)
% axis equal

%% OUTPUT
eos = [];
Type = 'simple';
Name = 'SW';
% load state at end time
load([outputdir,'/state_',Name,'.mat'])
%% The fluid
fluid = getCO2Brine('fluidprop','corey-brooks','pvt',Type,'eos',eos,...
    'Pe',1e3, 'salinity', 0.0, 'cR',1.9875e6,'rho',[1000 1182 479],'n',[1 1 1]);
fluid_simple = initSimpleADIFluid( 'mu', [1 71.85 3.950]*1e-5, 'rho',[1000 1000 479]);
fluid.muO = @(p,T) fluid_simple.muO(p,T);
fluid.bO = @(p,T) fluid_simple.bO(p,T);

%%  Saturations
maxS = max(state.s(:,3));
minS = min(state.s(:,3));
% cstep = min(0.05,maxS/2);
% clevs = minS:cstep:maxS;

figure(1)
clf 
colormap jet
plotSlice(G,state.s(:,3),1,2); ax = gca;
set(ax, 'Xdir', 'reverse')
xlabel('x [m]'); 
zlabel('z [m]');
hc = colorbar('v');
clevs = get(hc,'YTick');
clevs = unique([maxS,minS,clevs]);
set(hc,'YTick',clevs)
set(get(hc,'xlabel'),'String','S_{ CO_{2}}')
savefig([outputdir,'/sat_',Name,'.fig'])

if editflag
    box on
    set(ax,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'ZTick',500:100:1000, ...
        'FontWeight','bold','LineWidth',2.0)
    set(hc,'YTick',round(clevs*100)/100)
    set(hc,'ticklength',0.02)
    set(get(hc,'xlabel'),'String','')
    saveas(gcf,[saveasdir,'/sat_',Name],format)
    export_fig([saveasdir,'/sat_',Name],format2,'-nocrop',false,...
    '-transparent', '-q100')
end

%% Temperature
maxT = max(state.T);
minT = min(state.T); 
% clevs = minT:0.1:maxT;
% clevs = unique([clevs,maxT]);
% clevs = floor(clevs*10)/10;

figure(2)
clf  
colormap jet
plotSlice(G,state.T,1,2); ax = gca;
set(ax, 'Xdir', 'reverse')
xlabel('x [m]'); zlabel('z [m]');
hc = colorbar('v');
clevs = get(hc,'YTick');
clevs = unique([maxT,minT,clevs]);
set(hc,'YTick',clevs)
set(get(hc,'xlabel'),'String','T [K]')
savefig([outputdir,'/temp_',Name,'.fig'])

if editflag
    box on
    set(ax,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'ZTick',500:100:1000, ...
        'FontWeight','bold','LineWidth',2.0)
    
    set(hc,'YTick',305.5:0.5:clevs(end))
    set(hc,'ticklength',0.02)
    set(get(hc,'xlabel'),'String','')
    saveas(gcf,[saveasdir,'/temp_',Name],format)
    export_fig([saveasdir,'/temp_',Name],format2,'-nocrop',false,...
    '-transparent', '-q100')
end

%% Pressure (wetting-phase)
p = state.pressure/mega;% + fluid.pcOG(state.s(:,2))/mega;
maxP = max(p);
minP = min(p);
% maxP = floor(max(p)*100)/100;
% minP = ceil(min(p)*100)/100;
% cstep = 0.5;
% start = floor(minP)+cstep;
% fin = floor(maxP);
% if (fin+cstep<maxP), fin = fin+cstep; end
% if (start>minP), start = start-cstep; end
% % clevs = unique([minP,start:cstep:fin,maxP]);
% clevs = unique(start:cstep:fin);

figure(3)
clf  
colormap jet
plotSlice(G,p,1,2); ax = gca;
set(ax, 'Xdir', 'reverse')
xlabel('x [m]'); zlabel('z [m]');
hc = colorbar('v');
clevs = get(hc,'YTick');
clevs = unique([maxP,minP,clevs]);
set(hc,'YTick',clevs)
set(get(hc,'xlabel'),'String','p [MPa]')
savefig([outputdir,'/press_',Name,'.fig'])

if editflag
    box on
    set(ax,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'ZTick',500:100:1000, ...
        'FontWeight','bold','LineWidth',2.0)
    set(hc,'YTick',clevs(2:end-1))
    set(hc,'ticklength',0.02)
    set(get(hc,'xlabel'),'String','')
    saveas(gcf,[saveasdir,'/press_',Name],format)
    export_fig([saveasdir,'/press_',Name],format2,'-nocrop',false,...
    '-transparent', '-q100')
end

%% Density
p = state.pressure + fluid.pcOG(state.s(:,2));
rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
minR = min(rhoG); maxR= max(rhoG);
% minR = ceil(min(rhoG)); maxR= floor(max(rhoG));
rhoG(state.s(:,3)<largeEps) = 0;
% cstep = 50;
% start = floor(minR/100)*100+cstep;
% fin = floor(maxR/100)*100;
% if (start<minR), start = start+cstep; end
% if (fin>maxR), fin = fin - cstep; end
% % clevs = round([minR, start:cstep:fin, maxR]);
% clevs = round([start:cstep:fin, fin+cstep]);
% clevs =unique([0,clevs]);

figure(4)
clf 
colormap jet 
plotSlice(G,rhoG,1,2); ax = gca;
set(ax, 'Xdir', 'reverse')
xlabel('x [m]'); zlabel('z [m]');
hc = colorbar('v');
clevs = get(hc,'YTick');
caxis([minR,maxR])
clevs = unique([maxR,minR,clevs]);
set(hc,'YTick',clevs)
set(get(hc,'xlabel'),'String','\rho [kg/m3]')
savefig([outputdir,'/den_',Name,'.fig'])

if editflag
    box on
    set(ax,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'ZTick',500:100:1000, ...
        'FontWeight','bold','LineWidth',2.0)
    clevs(3) = ceil(clevs(3));
    clevs(end) = floor(clevs(end));
    set(hc,'YTick',clevs)
    set(hc,'ticklength',0.02)
    set(get(hc,'xlabel'),'String','')
    saveas(gcf,[saveasdir,'/den_',Name],format)
    export_fig([saveasdir,'/den_',Name],format2,'-nocrop',false,...
    '-transparent', '-q100')
end

%% Phase diagrams
% figure(5)
% clf
% hold on
% p = state.pressure + fluid.pcOG(state.s(:,2)); 
%  P = reshape(p,nx,nz)'/mega;
% %  rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
% %  R = reshape(rhoG,nx,nz)';
%  T = reshape(state.T,nx,nz)';
%  S = reshape(state.s(:,3),nx,nz)';
%  for i = 2:size(T,2)
%      bool = (S(:,i)>1e-12);
%     if all(~bool), break; end
%     plot(T(bool,i),P(bool,i),'b'), drawnow
%  end
%  bool = (S(:,1)>1e-12);
%  plot(T(bool,1),P(bool,1),'r--','LineWidth',2.0), drawnow
%  xtick = get(gca,'XTick');
%  plot(xtick,7.83*ones(size(xtick)),'k')
% % Along the horizontal line (at the injection level)
% k = find(Z(1,:)<=-900,1,'first');
% % bool = (S(k,:)>1e-12);
% % plot(T(k,bool),P(k,bool),'g')
% savefig([outputdir,'/phaseDiags.fig'])
% 
% if editflag
%     box on
%     set(ax,'FontSize',24,'TitleFontWeight','bold',...
%         'XTick',0:100:500, 'ZTick',500:100:1000, ...
%         'FontWeight','bold','LineWidth',2.0)
%     saveas(gcf,[saveasdir,'/phaseDiags',Name],format)
% end







