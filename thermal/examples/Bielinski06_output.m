%% Example by A. Bielinski (2006)

mrstModule add ad-fi deckformat ad-props
% workspace_path = '~/workspace/mrst-thermal/';
% addpath(genpath([workspace_path,'thermal/util']))
% addpath(genpath([workspace_path,'thermal/simulator']))
addpath(genpath('~/Documents/MATLAB/thermal'))

%% INPUT
clc
clear

load('axes4');  % xx, yy, zz, depth, Ix, right, bottom, inj 

Iz = Ix + depth; Iy = yy'; % Physical dimensions of reservoir

mrstVerbose(1);
outputdir = '~/workspace/thermal/test';

perm    = 1.0 * darcy;            % Permeability 
phi        = 0.2;             % Porosity

nx = size(xx,1)-1;
ny = size(yy,1)-1;
nz = size(zz,1)-1;

largeEps = 1e-7;

gravity('on')

 restart = 0; 

%% The grid

G = tensorGrid(xx,yy,zz,'depthz',repmat(depth,nx+1,ny+1));
G = computeGeometry(G);
nc_tot = G.cells.num;
rock.perm=perm*ones(G.cells.num,1);
rock.poro=phi*ones(G.cells.num,1);
%%
figure(1), clf
plotGrid(G, 'FaceColor', 'yellow', 'EdgeAlpha', 0.5), view(-20,20)
axis equal

%% The fluid
fluid_simple = initSimpleADIFluid( 'mu', [1 71.85 3.950]*1e-5, 'rho',[1000 1000 479]);
fluid = getCO2Brine('fluidprop','corey-brooks','pvt','simple',...
    'Pe',1e3, 'salinity', 0.0, 'cR',1.9875e6,'rho',[1000 1182 479],'n',[1 1 1]);
fluid.muO = @(p,T) fluid_simple.muO(p,T);
fluid.bO = @(p,T) fluid_simple.bO(p,T);

%% initialize

ipress     = 5.101325e6 * Pascal; % Initial pressure
itemp      = 37 + 273.15;    % Initial temperature
ir = 1.25 * kilogram / second; % Injection rate 
injtemp = 37 + 273.15; % Injected temperature

% Hydrostatic pressure function (of depth z)
grav = gravity;
grav = grav(3);
topPressure = ipress;
pressure = @(z) topPressure - fluid.rhoOS * (depth-z)*grav;

%% BC
% G.faces.tag(G.faces.centroids(:,1) == 0,:) = -1; 
% G.faces.tag(G.faces.centroids(:,1) == Lx,:) = -2;
% G.faces.tag(G.faces.centroids(:,2) == 0,:) = -3; 
% G.faces.tag(G.faces.centroids(:,2) == Ly,:) = -4; 
rightside = G.faces.centroids(:,1) > (Ix(end) - largeEps);
bottomside = G.faces.centroids(:,3) > (Iz(end) - largeEps);
G.faces.tag(rightside) = -2;
G.faces.tag(bottomside) = -6;

%% schedule

endTime = 30 * day;
dtmin = (1e-6) * day;
dtmax = (1e-2) * day;
nsub = log(4*dtmax/dtmin);
substeps = dtmin * exp(linspace(0,nsub,1000));  % increasing sequence from dtmin to dtmax
% dt = (1e-4) * day;
nsteps = ceil((endTime-sum(substeps))/dtmax);
dtmax = (endTime-sum(substeps))/nsteps;
dt = [0; substeps'; dtmax*ones(nsteps,1)];
nsteps = numel(dt);

%  restart = nsteps;
%% OUTPUT
%


addpath(genpath(outputdir))


%%

[X,Z] = meshgrid(G.faces.centroids(bottomside,1),G.faces.centroids(rightside,3));
X = X';
Z = -Z';
srange = unique([0:100:4897,4897]);

%%  Saturations
load(['state',sprintf('%05.0f.mat',srange(end))])
maxS = max(state.s(:,3));
minS = min(state.s(:,3));
cstep = min(0.06,maxS/2);
clevs = minS:cstep:maxS;
figure(3)
clf 
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
set(get(hc,'xlabel'),'String','S_{ CO_{2}}')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
for i = 2:length(srange)
    load(['state',sprintf('%05.0f.mat',srange(i))],'state')
    S = reshape(state.s(:,3),size(X));
    contourf(X,Z,S,clevs), drawnow
%     contourf(X,Z,S,clevs,'LineColor', 'none'), drawnow
    minS = min(minS,min(state.s(:,3)));
    maxS = max(maxS,max(state.s(:,3)));
end
set(gca,'YTickLabel', -get(gca,'YTick'))

%% Temperature
load(['state',sprintf('%05.0f.mat',srange(end))])
maxT = max(state.T);
minT = min(state.T); 
% cstep = 1;
% clevs = round(minT:cstep:maxT);
clevs = linspace(minT,maxT,11);
clevs = floor(clevs*10)/10;
% clevs(2:end-1) = round(clevs(2:end-1)*10)/10;

figure(4)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs)
set(get(hc,'xlabel'),'String','T [K]')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
for i = 2:length(srange)
    load(['state',sprintf('%05.0f.mat',srange(i))])
    T = reshape(state.T,size(X));
    contourf(X,Z,T,clevs), drawnow
%     contourf(X,Z,T,clevs,'LineColor', 'none'), drawnow
    maxT = max(maxT,max(state.T));
    minT = min(minT,min(state.T));
end
ytick = get(gca,'YTick');
set(gca,'YTickLabel', -ytick)

%% Pressure
load(['state',sprintf('%05.0f.mat',srange(end))])
p = state.pressure;
maxP = max(p)/mega;
minP = min(p)/mega;
cstep = 0.5;
start = floor(minP)+cstep;
fin = floor(maxP);
if (fin+cstep<maxP), fin = fin+cstep; end
clevs = unique([minP,start:cstep:fin,maxP]);

figure(5)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs)
set(get(hc,'xlabel'),'String','Pw [MPa]')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
for i = 1:length(srange)
    load(['state',sprintf('%05.0f.mat',srange(i))])
    p = state.pressure;
    P = reshape(p/mega,size(X));
    contourf(X,Z,P,clevs,'LineColor', 'none'), drawnow
%     maxP = max(maxP,max(p)/mega);
%     minP = min(minP,min(p)/mega);
end
set(gca,'YTickLabel', -get(gca,'YTick'))

%% Density
load(['state',sprintf('%05.0f.mat',srange(end))])
p = state.pressure + fluid.pcOG(state.s(:,2));
rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
minR = min(rhoG); maxR= max(rhoG);

% clevs = linspace(minR,maxR,20);
cstep = 50;
start = floor(minR/100)*100+cstep;
fin = floor(maxR/100)*100;
clevs = round([minR, start:cstep:fin, maxR]);
clevs =unique([0,clevs]);

figure(6)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs)
set(get(hc,'xlabel'),'String','   \rho  [kg/m3]')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
for i = 2:length(srange)
    load(['state',sprintf('%05.0f.mat',srange(i))])
    p = state.pressure + fluid.pcOG(state.s(:,2));
    rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
    minR = min(minR,min(rhoG)); 
    maxR= max(maxR,max(rhoG));
    rhoG(state.s(:,3)<largeEps) = 0;
    R = reshape(rhoG,size(X));
    contourf(X,Z,R,clevs), drawnow
%     contourf(X,Z,R,clevs,'LineColor', 'none'), drawnow
end
set(gca,'YTickLabel', -get(gca,'YTick'))

%%
figure(9)
clf
hold on
% for i = 2:length(srange)
%     load(['state',sprintf('%05.0f.mat',srange(i))])
p = state.pressure + fluid.pcOG(state.s(:,2));
 P = reshape(p,size(X))';
 T = reshape(state.T,size(X))';
 S = reshape(state.s(:,3),size(X))';
 for i = 1:size(T,2)
     bool = (S(:,i)>largeEps);
     if (all(~bool))
         break;
     end
    plot(T(bool,i),P(bool,i)/mega,'b'), drawnow
 end
 bool = (S(:,1)>largeEps);
 plot(T(bool,1),P(bool,1)/mega,'r'), drawnow
 bool = (S(:,i)>largeEps);
 plot(T(bool,i),P(bool,i)/mega,'k'), drawnow
%  plot(T(:,1),7.83*ones(size(T(:,1))),'--')
%  plot(304.16*ones(size(P(:,1))),P(:,1)/1e6,'--')
% end


