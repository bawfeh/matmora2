%% Benchmark 1 Class_Ebigo_etal_09

mrstModule add ad-fi deckformat ad-props
workspace_path = '~/workspace/mrst-thermal/';
addpath(genpath([workspace_path,'thermal/util']))
addpath(genpath([workspace_path,'thermal/simulator']))

%% INPUT
[nx,ny,nz] = deal(20, 20, 20);   % Cells in Cartsian grid



x_n = 5; % 8
leak_n = 5; % 8
well_n = 5; % 9
% 8 8 9 == used for isothermal case

[Lx,Ly,H]  = deal(1000,1000,160); % Physical dimensions of reservoir
%total_time = 1 * day;          % Total simulation time
%nsteps     = 10;                 % Number of time steps in simulation
%dt         = total_time/nsteps;   % Time step length

mrstVerbose(1);
outputdir = '/home/thsa/workspace/mrst-thermal/thermal/test';

% dt = [0.0005 * day * ones(100,1); ... % 0-0.05day 
%       0.001 * day * ones(150,1); ...% 0.005-0.2 days
%       0.005 * day * ones(60,1); ... % 0.2-0.5 days 
%       0.01 * day * ones(150,1); ... % 0.5-2 days
%       0.05 * day * ones(160,1); ...% 2-10 days
%       0.1 * day * ones(400,1); ... %10-50 days 
%       0.5 * day * ones(300,1); ... %50-200 days
%       1 * day * ones(1800,1)]; % 200 - 2000

 %dt = [10.^(-4:0.25:-2)'; 0.0072; 0.01 * ones(17,1); 0.05 * ones(998*2,1); 0.1 *ones(19000,1)] * day; % 100 days

 %dt = ones(2000,1) * day; % 1000 days

 % 44 days (0.1) + 6 days (0.02) + 16 days 0.1 + 4 days (0.02) + 30 day (0,01)
 %dt = [10.^(-6:0.25:-2)'; 0.0072; 0.01 * ones(17,1); 0.1 * ones(398+40,1); 0.02 *ones(300,1); 0.1 *ones(160,1); ...
 %    0.02 *ones(200,1);0.05 *ones(600,1); 0.1 * ones(100,1);0.1 * ones(500,1); 0.1 * ones(18400,1)] * day; % 100 days + 10 + 50 + 840

 %dt = [10.^(-4:0.25:-2)'; 0.0073; 0.01 * ones(17,1);0.1 * ones(58,1);1 *ones(1994,1)]*day;
 %dt = [10.^(-6:0.20:-2)'; 0.0073; 0.01 * ones(17,1);0.05 * ones(2400*1,1)]*day;
 dt = [10.^(-6:0.25:-2)'; 0.0073; 0.01 * ones(17,1);0.02 * ones(5990*1,1); 0.2 * ones(9400,1)]*day; % 120days + 1800 days
 %dt = 0.1 * ones(10,1) * day;
 %restart = 2754; %1933;
 %dt = [dt(1:2754); ones(1827,1)*day];
 restart = 2; % 10210; %9329;
%       0.88889 * day * ones(1,1); ... %10-50 days 
%       1 * day * ones(99,1); ... %50-200 days

%       1 * day * ones(1800,1)]; % 200 - 2000

%dt = [0.000001 * day * ones(10,1); ...
%    1 * day * ones(99,1)];
% dt = [0.0000001 * day * ones(10,1); ... % 0-0.05day 
 %        0.000001 * day * ones(10,1); ...]; % 200 - 2000
  %       1 * day * ones(999,1)];

  %dt = 0.1 * day * ones(21,1);
  nsteps = numel(dt);

dr = 0.15;

perm       = 2e-14;            % Permeability 
perm_leakywell = 1e-12;        % Permeability of the leaky well
phi        = 0.15;             % Porosity
depth      = 640;             % Initial depth
ipress     = 8.4991e6 * Pascal; % Initial pressure
itemp      = 34 + 273.15;    % Initial temperature
ir = 8.87 * kilogram / second; % Injection rate 
injtemp = 33.6 + 273.15; % Injected temperature

gravity('on')

%% The grid

layers = depth + [0 30 130 160];  

xx = [ linspace(0,Lx/2-dr,nx/2), linspace(Lx/2 + dr, Lx, nx/2)] ;
yy = [ linspace(0,Ly/2-dr,ny/2), linspace(Ly/2 + dr,Ly, ny/2)];
zz = unique([ linspace(0,30,5), linspace(30,130,10),linspace(130,160,5)]);

if (1)
    load('axes.mat','zz');
    zz = zz(zz<30 | zz>130);
    L = sqrt(pi)*dr;
    
    rfleak = logspace(log10(L/2),log10(50),leak_n);
    rfleak = rfleak(1:end-1);
    rfwell = logspace(log10(L*2),log10(50),well_n);
    rfwell = rfwell(1:end-1);
    xx = [ linspace(0,350,x_n), fliplr(400 - rfwell), 400 + rfwell, 450, fliplr(500 - rfleak), ...
        500 + rfleak , linspace(550, Lx, x_n+1)] ;
    yy = xx;
    
    
    
    % remove refinement around injector. 
    %x = [linspace(0,450,15), xx(33:end)];
    
    nx = numel(xx);
    ny = numel(yy);
    nz = numel(zz);
end
G = tensorGrid(xx,yy,zz,'depthz',depth*ones(nx*ny,1));
G = computeGeometry(G);

aquitard = G.cells.centroids(:,3) > layers(2) & G.cells.centroids(:,3) < layers(3);
leak = G.cells.centroids(:,1) == 500 & G.cells.centroids(:,2) == 500;

nc_tot = G.cells.num;
map = zeros(nc_tot,1);
G = removeCells(G,aquitard & ~leak);
map(G.cells.indexMap) = 1:G.cells.num;

leak = map(leak);
leak = leak(leak>0);

rock.perm=perm*ones(G.cells.num,1);
rock.perm(leak) = perm_leakywell;
rock.poro=phi*ones(G.cells.num,1);

%% The fluid
%fluid = initSimpleADIFluid('mu', [1 25.35 3.950]*1e-5, 'rho', [1000 1045 479], 'n', [1 1 1]);
fluid = getCO2Brine('fluidprop','corey-brooks','pvt','simple','cR',1.9875e3,'rho',[1000 1000 2],'n',[1 1 1]);

%% ADI
system = initADISystem({'Oil','Water','Gas'}, G, rock, fluid, 'cpr', false);
system.stepFunction =@ stepVOTcW;
system.getEquations =@ eqsfiVOTcW;
system.updateState  =@ updateStateVOTcW;
%system.stepFunction =@ stepBlackOilTemp;
%system.getEquations =@ eqsfiBlackOilTempMod;
%system.updateState  =@ updateStateBlackOilTempMod;
system.fluid = fluid;       
system.nonlinear.use_ecltol = false;

%% initialize

% Pressure function
grav = gravity;
pressure = @(z) ipress - fluid.rhoOS*((layers(4))-z)*grav(3);

state.pressure = pressure(G.cells.centroids(:,3));
state.s = zeros(G.cells.num,3);
state.s(:,2) = 1;
state.s(:,1) = 0;
state.T = itemp + 0.03 * (G.cells.centroids(:,3)-layers(4));

if (system.activeComponents.disgas)
    state.rs = zeros(G.cells.num,1); 
else
    state.rs = 0;
end

state.rv = 0; % or 
%% The Well
W=[];
wc = findClosestCell(G,400,500,0) + (0:G.cartDims(3)-1)'* (G.cartDims(1)) * (G.cartDims(2));
wc_map = map(wc);
wc_map = wc_map(wc_map>0);
wc_map =  wc_map( G.cells.centroids(wc_map,3) > (layers(3)) );

ir_v = ir ./ fluid.rhoGS;
W = addWell(W, G, rock,  wc_map,     ...
                     'Type', 'rate', 'Val', ir_v,'refDepth',layers(3), ...
                     'Radius', 0.15, 'Name', 'I1','Comp_i',[0 0 1],'sign',1);
%figure, plotGrid(G),plotWell(G,W);

ir_vO = ir ./ fluid.rhoOS;

%state.s(wc_map,1) = 0.01;
%state.s(wc_map,2) = 0.99;
%% Thermal part
fake_rock.perm = 3.5*ones(G.cells.num,1); % J / s*m*K
T = computeTrans(G,fake_rock);
Trans=1./accumarray(G.cells.faces(:,1),1./T,[G.faces.num,1]);
internal=all(G.faces.neighbors>0,2);

system.s.T_r = Trans(internal);
%system.s.iT = 33.6 + 273.15;


%% BC
G.faces.tag(G.faces.centroids(:,1) == 0,:) = -1; 
G.faces.tag(G.faces.centroids(:,1) == Lx,:) = -2;
G.faces.tag(G.faces.centroids(:,2) == 0,:) = -3; 
G.faces.tag(G.faces.centroids(:,2) == Ly,:) = -4; 

bc = [];
for i = 1:4
    bc = addBC(bc, find(G.faces.tag == -i), 'pressure', pressure(G.faces.centroids(G.faces.tag == -i,3)));
    
end
bc=bc2ADbc(G,bc);
system.bc = bc;

for i=1:numel(W)
    % black oil relatetd
   W(i).bhpLimit = inf;
   if(W(i).sign>0)
       W(i).lims=struct('bhp',inf','rate',inf,'orat',inf,'wrat',inf,'grat',inf,'lrat',inf);
   else
       W(i).lims=struct('bhp',-inf','rate',-inf,'orat',-inf,'wrat',-inf,'grat',-inf,'lrat',-inf);
   end
   %W(i).refDepth = G.cells.centroids(W(i).cells(1),3);
   W(i).T = '';
   %W(i).dZ= 0 * ones(numel(W(i).cells),1);
   
end
W(1).T = 33.6 + 273.15;
%% schedule
mrst_schedule.step = struct('control',ones(nsteps,1),'val',dt);

mrst_schedule.control(1).W = W;

if restart>0
    load([outputdir,'/state',sprintf('%05.0f',restart)]);
end
%mrst_schedule = struct('step',struct('control',ones(nsteps,1),'val',dt));
%mrst_schedule.control.W=W;
%mrst_schedule.W = {W};

%% RUN
[wellstates,states, iter] = runScheduleADI(state, G, rock, system, mrst_schedule,'writeOutput',true,'outputdir',outputdir,'startAt',restart);
%runScheduleADI(state, G, rock, system, mrst_schedule,'writeOutput',true,'outputdir',outputdir,'restart',restart);

%[wellstates,states, iter] = runMrstADI(state, G, system, mrst_schedule,'writeOutput', false, 'force_step',true);

 
%% OUTPUT
%pick = findClosestCell(G,500,500,80+depth);
 pv = poreVolume(G,rock);
 pick = G.cells.centroids(:,3) < (80 + depth);
 mass_co2 = zeros(numel(states),1);
 for i = 1:numel(states)
     state = states{i};
     s_co2 = state.s(pick,3);
     rho = fluid.rhoGS;
     mass_co2(i) = sum( s_co2.*rho.*pv(pick) );
 end
pick = findClosestFace(G,500,500,80+depth);
for i = 2:numel(states)
    state = states{i};
    flux(i) = state.bGvG.val(pick);
end
accuminj = ir * cumsum(mrst_schedule.step.val);
accummass = cumsum(mrst_schedule.step.val'.*flux(2:nsteps+1))';

figure, plot(mass_co2(2:end)./accuminj * 100);
figure, plot(diff(mass_co2)./(ir*day) * 100);
return
%%
figure, 
for i = 1:numel(states)
    plotCellData(G,states{i}.pressure),caxis([min(states{end}.pressure),max(states{end}.pressure)]),colorbar,view(3);
    pause
end

%%
figure, 
for i = 1:numel(states)

    pause
    
    
end

%%
 pv = poreVolume(G,rock);
 pick = G.cells.centroids(:,3) < (30 + depth);
 %pick_c = findClosestFace(G,500,500,layers(2));
 %%pick = 1:G.cells.num;
 pick_w = findClosestCell(G,500,500,layers(2)+50);
 n = 15425;
 mass_co2 = zeros(n+1,1);
 mass_dco2 = mass_co2;
 inj_mass = mass_co2;
 time = cumsum(dt)/day;
 j = 1;
 dn = 100;
 for i = 1:dn:n
     load(['state',sprintf('%05.0f',i)]);
     %state = states{i};
     s_co2 = state.s(pick,3);
     s_b = state.s(pick,2);
     p = state.pressure(pick);
     T = state.T(pick);
     rs = state.rs(pick);
     
     V_co2_sc = fluid.bG(p,T).*pv(pick).*s_co2;
     V_b_sc = fluid.bO(p,rs,rs==fluid.rsSat(p,T),T).*pv(pick).*s_b;
     mass_co2(j) = sum(fluid.rhoGS.*V_co2_sc);
     mass_dco2(j) = sum(fluid.rhoGS.*rs.*V_b_sc);
         
     %rho = fluid.bG(state.pressure,state.T).*(fluid.rhoGS);
     %rhoGinO = fluid.bO(state.pressure,state.rs,state.rs==fluid.rsSat(state.pressure,state.T),state.T).*state.rs.*fluid.rhoGS;
     %mass_co2(j) = sum( state.s(pick,3).*rho(pick).*pv(pick));
     inj_mass(j) = ir*time(i) ;
     j = j + 1;
     %mass_co22(j) = sum( state.s(pick,2).*rhoGinO(pick).*pv(pick));
     flux(j) = state.gF(pick_c);
     states{i} = state;
 end
  
%%
figure
plot(time(1:dn:n), mass_co2(1:j-1)./inj_mass(1:j-1)/day * 100,'m-','linewidth', 2); hold all
plot(time(1:dn:n), mass_dco2(1:j-1)./inj_mass(1:j-1)/day * 100,'g-','linewidth', 2);
plot(time(1:dn:n), (mass_co2(1:j-1) + mass_dco2(1:j-1))./inj_mass(1:j-1)/day * 100,'b-','linewidth', 2);
legend({'free-co2-iso','dissolved-co2-iso','total-co2-iso'})


%figure, plot(time(1:10:n), -flux(2:j)./(ir*day) * 100);
arrivaltime = time(min(find(mass_co2(1:j)>0.005))*dn)
[maxrate,i] = max((mass_co2(1:j-1) + mass_dco2(1:j-1))./inj_mass(1:j-1)/day * 100)
time_maxrate = time(i*dn)

return
%%
isothermal.mass_co2 = mass_co2;
isothermal.inj_mass = inj_mass;
isothermal.mass_dco2 = mass_dco2;
isothermal.time = time;

thermal.mass_co2 = mass_co2;
thermal.inj_mass = inj_mass;
thermal.mass_dco2 = mass_dco2;
thermal.time = time;
%%
figure

axes('FontSize',20);

plot(isothermal.time(1:dn:n), isothermal.mass_co2(1:j-1)./isothermal.inj_mass(1:j-1)/day * 100,'m-','linewidth', 2); hold all
plot(isothermal.time(1:dn:n), isothermal.mass_dco2(1:j-1)./isothermal.inj_mass(1:j-1)/day * 100,'r-','linewidth', 2);
plot(isothermal.time(1:dn:n), (isothermal.mass_co2(1:j-1) + isothermal.mass_dco2(1:j-1))./isothermal.inj_mass(1:j-1)/day * 100,'b-','linewidth', 2);

plot(thermal.time(1:dn:n), thermal.mass_co2(1:j-1)./thermal.inj_mass(1:j-1)/day * 100,'m+','markersize', 8); hold all
plot(thermal.time(1:dn:n), thermal.mass_dco2(1:j-1)./thermal.inj_mass(1:j-1)/day * 100,'r+','markersize', 8);
plot(thermal.time(1:dn:n), (thermal.mass_co2(1:j-1) + thermal.mass_dco2(1:j-1))./thermal.inj_mass(1:j-1)/day * 100,'b+','markersize', 8);

legend({'Free CO2 (iso-thermal)','Dissolved CO2 (iso-thermal)','Total CO2 (iso-thermal)', ...
    'Free CO2','Dissolved CO2','Total CO2'},'fontsize',20)
xlabel( 'time [Days]','fontsize',20)
ylabel('leakage value [%]','fontsize',20)



