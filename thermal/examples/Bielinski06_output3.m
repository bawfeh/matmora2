%% Example by A. Bielinski (2006)

% mrstModule add ad-fi deckformat ad-props
% % workspace_path = '~/workspace/mrst-thermal/';
% % addpath(genpath([workspace_path,'thermal/util']))
% % addpath(genpath([workspace_path,'thermal/simulator']))
% addpath(genpath('~/Documents/MATLAB/thermal'))
% addpath('~/Documents/MATLAB/export_fig')

%% INPUT
clc
clear

load('axes4');  % xx, yy, zz, depth, Ix, right, bottom, inj 

Iz = Ix + depth; Iy = yy'; % Physical dimensions of reservoir

mrstVerbose(1);

perm    = 1.0 * darcy;            % Permeability 
phi        = 0.2;             % Porosity

nx = size(xx,1)-1;
ny = size(yy,1)-1;
nz = size(zz,1)-1;

largeEps = 1e-7;

gravity('on') 

%% The grid

G = tensorGrid(xx,yy,zz,'depthz',repmat(depth,nx+1,ny+1));
G = computeGeometry(G);
nc_tot = G.cells.num;
rock.perm=perm*ones(G.cells.num,1);
rock.poro=phi*ones(G.cells.num,1);
%%
% figure(1), clf
% plotGrid(G, 'FaceColor', 'yellow', 'EdgeAlpha', 0.5), view(-20,20)
% axis equal

%%
rightside = G.faces.centroids(:,1) > (Ix(end) - largeEps);
bottomside = G.faces.centroids(:,3) > (Iz(end) - largeEps);
[X,Z] = meshgrid(G.faces.centroids(bottomside,1),G.faces.centroids(rightside,3));
X = X';
Z = -Z';

%% OUTPUT
%
% load state at end time

EOS = cell(1,7);
eos = [];
eos.parameters = struct('delta1',-1,'delta2',0,'kappa',0.8263,...
            'omegaA',0.4275,'omegaB',0.0866); % SRK
EOS{3} = eos;
eos.parameters = struct('delta1', -4.9009,'delta2',-0.0124,'kappa',0.7056,...
            'omegaA',0.4572,'omegaB',0.0450);  % volume-translated PR with correct Zc = 0.2746
EOS{4} = eos;
eos.parameters = struct('delta1', -3.1127,'delta2',0.2944,'kappa',0.7056,...
            'omegaA',0.4572,'omegaB',0.0646); % optimal volume-translated PR
EOS{5} = eos;
eos.parameters = struct('delta1', -3.7507,'delta2',0.8536,'kappa',0.3759,...
            'omegaA',0.5479,'omegaB',0.0929);  % optimal with correct Zc = 0.2746
EOS{6} = eos;
eos.parameters = struct('delta1', -3.7046,'delta2',0.8259,'kappa',0.3488,...
            'omegaA',0.5364,'omegaB',0.0897);  % optimal
EOS{7} = eos;
Name = {'SW','PR','SRK','vtPRZc','vtPR','APRZc','APR'};
Legend = {'SW','PR','SRK','vtPRc','vtPR','Gc','G'};
Type = {'simple','cubic','cubic','cubic','cubic','cubic','cubic'};


editflag = true;
outputdir = '~/Documents/MATLAB/thermal/examples/outputT307axes4';
saveasdir = '~/Documents/Notes/ComputGeo';
format = 'fig';
format2 = '-pdf';

%% Extract data for comparisons
Sz = cell(1,7); 
Rz = cell(1,7);
Tz = cell(1,7); 
Pz = cell(1,7);
Sx = cell(1,7); 
Rx = cell(1,7);
Tx = cell(1,7); 
Px = cell(1,7);
Sb = cell(1,7);
% Along the horizontal line (at the injection level)
k = find(Z(1,:)<=-900,1,'first');
clevs = [0.05,1];
figure(9), clf, hold on
for method = 1:7
    % write data into workspace
    load([outputdir,'/state_',Name{method},'.mat'])
    % The fluid
    fluid = getCO2Brine('fluidprop','corey-brooks','pvt',Type{method},'eos',EOS{method},...
        'Pe',1e3, 'salinity', 0.0, 'cR',1.9875e6,'rho',[1000 1182 479],'n',[1 1 1]);
%     fluid_simple = initSimpleADIFluid( 'mu', [1 71.85 3.950]*1e-5, 'rho',[1000 1000 479]);
%     fluid.muO = @(p,T) fluid_simple.muO(p,T);
%     fluid.bO = @(p,T) fluid_simple.bO(p,T);
    % saturation
    S = reshape(state.s(:,3),size(X));
    Sz{method} = S(1,:);
%     Sx{method} = S(:,k);
    Sb{method} = contour(X,Z,S,clevs,'LineWidth',2.0, 'LineColor','b');
    if (method==1)
        sat_SW = S;
    end
    
    % density
    p = state.pressure + fluid.pcOG(state.s(:,2)); % non-wetting phase
    rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
    rhoG(state.s(:,3)<1e-12) = inf;
    S = reshape(rhoG,size(X))';
    Rz{method} = S(:,1);
    Rx{method} = S(k,:);
    % temperature
    S = reshape(state.T,size(X))';
    Tz{method} = S(:,1);
    Tx{method} = S(k,:);
    % pressure (wetting-phase)
    S = reshape(state.pressure/mega,size(X))';
    Pz{method} = S(:,1);
    Px{method} = S(k,:);
end
z = -Z(1,:)';
x = X(:,k);
hold off
    
%% Generate plots
purple = [0.5,0.0,0.5];
% darkgreen = [0.2,0.6,0.2];
darkgreen = [0.2,0.5,0.3];
% Along the vertical (left boundary)
%% saturations
figure(1)
clf
hold on
plot(z,Sz{1},'b','LineWidth',2.0), drawnow
plot(z,Sz{2},'k-.','LineWidth',1.0), drawnow
plot(z,Sz{3},'-','Color',purple,'MarkerSize',4,'LineWidth',1.0), drawnow
plot(z,Sz{4},'-o','MarkerSize',4,'Color',darkgreen,'LineWidth',1.0), drawnow
plot(z,Sz{5},':','Color',darkgreen,'MarkerSize',5,'LineWidth',2.0), drawnow
plot(z,Sz{6},'bs','MarkerSize',3,'LineWidth',1.0), drawnow
plot(z,Sz{7},'r--','LineWidth',1.0), drawnow
legend(Legend,'Location','best');
xlabel('z [m]')
ylabel('saturation')
savefig([outputdir,'Sz.fig'])
if editflag
    box on
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',500:100:1000, 'YTick',0:0.1:0.7,'XMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/Sz'],format)
    export_fig([saveasdir,'/Sz'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
hold off

%% density
figure(2)
clf
hold on
plot(z,Rz{1},'b', 'LineWidth',2.0), drawnow
plot(z,Rz{2},'k-.', 'LineWidth',2.0), drawnow
plot(z,Rz{3},'-','Color',purple,'MarkerSize',5, 'LineWidth',2.0), drawnow
plot(z,Rz{4},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',1.0), drawnow
plot(z,Rz{5},':','Color',darkgreen,'MarkerSize',5, 'LineWidth',2.0), drawnow
plot(z,Rz{6},'bs','MarkerSize',4, 'LineWidth',1.0), drawnow
plot(z,Rz{7},'r--', 'LineWidth',2.0), drawnow
legend(Legend,'Location','best');
xlabel('z [m]')
ylabel('density [kg/m3]')
savefig([outputdir,'Rz.fig'])
if editflag
    box on
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',500:100:1000, 'YTick',100:100:900,'XMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/Rz'],format)
    export_fig([saveasdir,'/Rz'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
hold off

%% temperature
figure(3)
clf
hold on
plot(z,Tz{1},'b', 'LineWidth',2.0), drawnow
plot(z,Tz{2},'k-.', 'LineWidth',2.0), drawnow
plot(z,Tz{3},'-','Color',purple,'MarkerSize',5, 'LineWidth',1.0), drawnow
plot(z,Tz{4},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',1.0), drawnow
plot(z,Tz{5},':','Color',darkgreen,'MarkerSize',4, 'LineWidth',2.0), drawnow
plot(z,Tz{6},'bs','MarkerSize',4, 'LineWidth',1.0), drawnow
plot(z,Tz{7},'r--', 'LineWidth',2.0), drawnow
legend(Legend,'Location','best');
xlabel('z [m]')
ylabel('temperature [K]')
savefig([outputdir,'Tz.fig'])
if editflag
    box on
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',500:100:1000, 'YTick',305:0.5:307.5,'XMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/Tz'],format)
    export_fig([saveasdir,'/Tz'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
hold off
%% pressure (wetting-phase)
figure(4)
clf
hold on
plot(z,Pz{1},'b', 'LineWidth',1.0), drawnow
plot(z,Pz{2},'k-.', 'LineWidth',2.0), drawnow
plot(z,Pz{3},'-','Color',purple,'MarkerSize',4, 'LineWidth',1.0), drawnow
plot(z,Pz{4},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',1.0), drawnow
plot(z,Pz{5},':','Color',darkgreen, 'LineWidth',2.0), drawnow
plot(z,Pz{6},'bs','MarkerSize',3, 'LineWidth',1.0), drawnow
plot(z,Pz{7},'r--', 'LineWidth',1.0), drawnow
legend(Legend,'Location','best');
xlabel('z [m]')
ylabel('pressure [MPa]')
savefig([outputdir,'Pz.fig'])
if editflag
    box on
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',500:100:1000, 'YTick',6:12,'XMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/Pz'],format)
    export_fig([saveasdir,'/Pz'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
hold off
%% Along the horizontal line (at injection)
% %% saturations
% figure(5)
% clf
% hold on
% plot(x,Sx{1},'b', 'LineWidth',2.0), drawnow
% plot(x,Sx{2},'k-.', 'LineWidth',2.0), drawnow
% plot(x,Sx{3},'-x','Color',purple,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Sx{4},'-.+','Color',darkgreen,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Sx{5},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',2.0), drawnow
% plot(x,Sx{6},'r--', 'LineWidth',2.0), drawnow
% plot(x,Sx{7},'r--s','MarkerSize',4, 'LineWidth',2.0), drawnow
% legend(Legend,'Location','best')
% xlabel('x [m]')
% ylabel('saturation')
% if editflag
%     box on
%     set(gca,'FontSize',24,'TitleFontWeight','bold',...
%         'XTick',0:100:500, 'YTick',0:0.1:0.7,'XMinorTick','on', ...
%         'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
%     saveas(gcf,[saveasdir,'/Sx'],format)
%     export_fig([saveasdir,'/Sx'],format2,'-nocrop',false,...
%     '-transparent', '-q100')
% end
% 
% %% density
% figure(6)
% clf
% hold on
% plot(x,Rx{1},'b', 'LineWidth',2.0), drawnow
% plot(x,Rx{2},'k-.', 'LineWidth',2.0), drawnow
% plot(x,Rx{3},'-x','Color',purple,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Rx{4},'-.+','Color',darkgreen,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Rx{5},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',2.0), drawnow
% plot(x,Rx{6},'r--', 'LineWidth',2.0), drawnow
% plot(x,Rx{7},'r--s','MarkerSize',4, 'LineWidth',2.0), drawnow
% legend(Legend,'Location','best')
% xlabel('x [m]')
% ylabel('density [kg/m3]')
% axis([0 500 600 900])
% savefig([outputdir,'Rx.fig'])
% if editflag
%     box on
%     set(gca,'FontSize',24,'TitleFontWeight','bold',...
%         'XTick',0:100:500, 'YTick',600:50:900,'XMinorTick','on','YMinorTick','on', ...
%         'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
%     saveas(gcf,[saveasdir,'/Rx'],format)
%     export_fig([saveasdir,'/Rx'],format2,'-nocrop',false,...
%     '-transparent', '-q100')
% end
% 
% %% temperature
% figure(7)
% clf
% hold on
% plot(x,Tx{1},'b', 'LineWidth',2.0), drawnow
% plot(x,Tx{2},'k-.', 'LineWidth',2.0), drawnow
% plot(x,Tx{3},'-x','Color',purple,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Tx{4},'-.+','Color',darkgreen,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Tx{5},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',2.0), drawnow
% plot(x,Tx{6},'r--', 'LineWidth',2.0), drawnow
% plot(x,Tx{7},'r--s','MarkerSize',4, 'LineWidth',2.0), drawnow
% legend(Legend,'Location','best')
% xlabel('x [m]')
% ylabel('temperature [K]')
% savefig([outputdir,'Tx.fig'])
% if editflag
%     box on
%     ylim([307 307.16])
%     set(gca,'FontSize',24,'TitleFontWeight','bold',...
%         'XTick',0:100:500, 'YTick',307:0.04:307.16,'XMinorTick','on','YMinorTick','on', ...
%         'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
%     saveas(gcf,[saveasdir,'/Tx'],format)
%     export_fig([saveasdir,'/Tx'],format2,'-nocrop',false,...
%     '-transparent', '-q100')
% end
% %% pressure (wetting-phase)
% figure(8)
% clf
% hold on
% plot(x,Px{1},'b', 'LineWidth',2.0), drawnow
% plot(x,Px{2},'k-.', 'LineWidth',2.0), drawnow
% plot(x,Px{3},'-x','Color',purple,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Px{4},'-.+','Color',darkgreen,'MarkerSize',4, 'LineWidth',2.0), drawnow
% plot(x,Px{5},'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',2.0), drawnow
% plot(x,Px{6},'r--', 'LineWidth',2.0), drawnow
% plot(x,Px{7},'r--s','MarkerSize',4, 'LineWidth',2.0), drawnow
% legend(Legend,'Location','best');
% xlabel('x [m]')
% ylabel('pressure [MPa]')
% savefig([outputdir,'Px.fig'])
% if editflag
%     box on
%     set(gca,'FontSize',24,'TitleFontWeight','bold',...
%         'XTick',0:100:500, 'YTick',9.9:0.1:10.4,'XMinorTick','on','YMinorTick','on', ...
%         'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
%     saveas(gcf,[saveasdir,'/Px'],format)
%     export_fig([saveasdir,'/Px'],format2,'-nocrop',false,...
%     '-transparent', '-q100')
% end
%% plume boundary
figure(9)
clf
hold on
colormap(jet(8))
contourf(X,Z,sat_SW,clevs, 'LineColor','b', 'LineWidth',1.0);
% plot(Sb{1}(1,2:end),Sb{1}(2,2:end),'b', 'LineWidth',2.0), drawnow
plot(Sb{2}(1,2:end),Sb{2}(2,2:end),'k-.', 'LineWidth',2.0), drawnow
plot(Sb{3}(1,2:end),Sb{3}(2,2:end),'-','Color',purple,'MarkerSize',4, 'LineWidth',1.0), drawnow
plot(Sb{4}(1,2:end),Sb{4}(2,2:end),'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',1.0), drawnow
plot(Sb{5}(1,2:end),Sb{5}(2,2:end),':','Color',darkgreen, 'LineWidth',2.0), drawnow
plot(Sb{6}(1,2:end),Sb{6}(2,2:end),'bs','MarkerSize',3), drawnow
plot(Sb{7}(1,2:end),Sb{7}(2,2:end),'r--', 'LineWidth',1.0), drawnow
Legend{1} = 'SW (CO_2 plume)';
hl = legend(Legend,'Location','southeast');
axis([0 500 -1000 -500])
set(gca,'YTicklabel',-get(gca,'YTick'))
box on
xlabel('x [m]')
ylabel('z [m]')
% hpos = get(gcf,'Position');
% htext = text(hpos(1)/2, hpos(2)/2,'CO_2 plume');
savefig([outputdir,'co2front.fig'])
if editflag
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'YTicklabel',1000:-100:500,'XMinorTick','on','YMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/co2front'],format)
    export_fig([saveasdir,'/co2front'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
Legend{1} = 'SW';
hold off
%%
figure(10)
clf
hold on
colormap(jet(8))
contourf(X,Z,sat_SW,clevs, 'LineColor','b', 'LineWidth',1.0);
% plot(Sb{1}(1,2:end),Sb{1}(2,2:end),'b', 'LineWidth',2.0), drawnow
plot(Sb{2}(1,2:end),Sb{2}(2,2:end),'k-.', 'LineWidth',2.0), drawnow
plot(Sb{3}(1,2:end),Sb{3}(2,2:end),'-','Color',purple,'MarkerSize',5, 'LineWidth',1.0), drawnow
plot(Sb{4}(1,2:end),Sb{4}(2,2:end),'-o','MarkerSize',4,'Color',darkgreen, 'LineWidth',1.0), drawnow
plot(Sb{5}(1,2:end),Sb{5}(2,2:end),':','Color',darkgreen, 'LineWidth',2.0), drawnow
plot(Sb{6}(1,2:end),Sb{6}(2,2:end),'bs','MarkerSize',3), drawnow
plot(Sb{7}(1,2:end),Sb{7}(2,2:end),'r--', 'LineWidth',1.0), drawnow
Legend{1} = 'SW (CO_2 plume)';
hl = legend(Legend,'Location','southeast');
axis([0 500 -1000 -500])
set(gca,'YTicklabel',-get(gca,'YTick'))
box on
xlabel('x [m]')
ylabel('z [m]')
% hpos = get(gcf,'Position');
% htext = text(hpos(1)/2, hpos(2)/2,'CO_2 plume');
savefig([outputdir,'co2front2.fig'])
if editflag
    ylim([-750,-500])
    ytick = -get(gca,'YTick');
    set(gca,'FontSize',18,'TitleFontWeight','bold',...
        'XTick',0:100:500, 'YTicklabel',ytick,'XMinorTick','on','YMinorTick','on', ...
        'ticklength',[0.02 0.05],'FontWeight','bold','LineWidth',2.0)
    saveas(gcf,[saveasdir,'/co2front2'],format)
    export_fig([saveasdir,'/co2front2'],format2,'-nocrop',false,...
    '-transparent', '-q100')
end
Legend{1} = 'SW';
hold off






