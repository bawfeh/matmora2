clc
clear
% P = (4.9:7.5e-3:12)'*mega;
% T = (300:0.04:310)';
% % P = (4.9:75e-3:12)'*mega;
% % T = (300:0.4:310)';
% 
% eos = [];
% 
% % eos.parameters = struct('delta1',-1-sqrt(2),'delta2',-1+sqrt(2),'kappa',0.7056,...
% %             'omegaA',0.45724,'omegaB',0.0778);  % PR
%         
% eos.parameters = struct('delta1',-1,'delta2',0,'kappa',0.8263,...
%             'omegaA',0.4275,'omegaB',0.0866); % SRK
% 
% % eos.parameters = struct('delta1', -3.7046,'delta2',0.8259,'kappa',0.3488,...
% %             'omegaA',0.5364,'omegaB',0.0897);  % optimal with Zc = 0.2772
% %         
% % eos.parameters = struct('delta1', -3.7507,'delta2',0.8536,'kappa',0.3759,...
% %             'omegaA',0.5479,'omegaB',0.0929);  % optimal with correct Zc = 0.2746
% %         
% % eos.parameters = struct('delta1', -4.9009,'delta2',-0.0124,'kappa',0.7056,...
% %             'omegaA',0.4572,'omegaB',0.0450);  % volume-translated PR with correct Zc = 0.2746
% %         
% % eos.parameters = struct('delta1', -3.1127,'delta2',0.2944,'kappa',0.7056,...
% %             'omegaA',0.4572,'omegaB',0.0646); % optimal volume-translated PR with  Zc = 0.2942
%       
% eos = updateEOS(eos);
% 
% [X,Y] = meshgrid(P,T);
% data = cubicEquationOfState(X,Y,eos);
% 
% [X,Y] = meshgrid(T,P/mega);
% clf, mesh(X,Y,data')
% xlim([min(T), max(T)])
% ylim([min(P),max(P)]/mega)
% 
% max(data(:)), min(data(:))
% 
% fluid.rhoGS = 1;
% fluid.bG = @(p,T) cubicEquationOfState(p,T,eos);
% mu = co2viscosity(X,Y,fluid);
% 
% figure(2),clf
% contourf(X,Y,mu)


outputdir = '~/Documents/Notes/ComputGeo/';
saveasdir = '~/Documents/Notes/ComputGeo/';
format = 'fig';
format2 = '-pdf';
editflag = true;
Name = 'pAllcentres';
% savefig([outputdir,Name,'.fig'])
% ylim([6 11.05]), xlim([302,308])
% xlabel('T [K]'), ylabel('P [MPa]')
if editflag
    box on
    set(gca,'FontSize',20,'TitleFontWeight','bold',...
        'XTick',0:500:2000, 'YTick',8.4:0.2:10.4,'XMinorTick','on', ...
        'FontWeight','bold','LineWidth',2.0,'ticklength',[0.02,0.05])
    saveas(gcf,[saveasdir,Name],format)
    export_fig([saveasdir,Name],format2,'-nocrop',false,...
    '-transparent', '-q100')
end



