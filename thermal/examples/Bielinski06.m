%% 2D Example by A. Bielinski (2006)

mrstModule add ad-fi deckformat ad-props
addpath(genpath('~/Documents/MATLAB/thermal'))

%% INPUT
clc
clear

load('axes4');  % xx, yy, zz, depth, Ix, right, bottom, inj 

Iz = Ix + depth; Iy = yy'; % Physical dimensions of reservoir

mrstVerbose(1);
outputdir = '~/workspace/thermal/test2';

perm    = 1.0 * darcy;            % Permeability 
phi        = 0.2;             % Porosity

nx = size(xx,1)-1;
ny = size(yy,1)-1;
nz = size(zz,1)-1;

largeEps = 1e-6;

gravity('on')

 restart = 0; 

%% The grid

G = tensorGrid(xx,yy,zz,'depthz',repmat(depth,nx+1,ny+1));
% G = cartGrid([100,1,100],[500,1.25,500],'depthz',repmat(depth,101,2));  inj = 100*(85:94)'+1;
% G = cartGrid([50,1,50],[500,0.625,500],'depthz',repmat(depth,51,2));  inj = 50*(43:47)'+1;
G = computeGeometry(G);
nc_tot = G.cells.num;
rock.perm=perm*ones(G.cells.num,1);
rock.poro=phi*ones(G.cells.num,1);
%%
% figure(1), clf
% plotGrid(G, 'FaceColor', 'yellow', 'EdgeAlpha', 0.5), view(-20,20)
% axis equal

%% The fluid
eos = [];
eos.parameters = struct('delta1', -4.9009,'delta2',-0.0124,'kappa',0.7056,...
            'omegaA',0.4572,'omegaB',0.0450);  % volume-translated PR with correct Zc

fluid = getCO2Brine('fluidprop','corey-brooks','pvt','cubic','eos',eos,...
    'Pe',1e3, 'salinity', 0.0, 'cR',1.9875e6,'rho',[1000 1182 479],'n',[1 1 1]);
fluid_simple = initSimpleADIFluid( 'mu', [1 71.85 3.950]*1e-5, 'rho',[1000 1000 479]);
fluid.muO = @(p,T) fluid_simple.muO(p,T);
fluid.bO = @(p,T) fluid_simple.bO(p,T);
%% ADI
system = initADISystem({'Oil','Water','Gas'}, G, rock, fluid, 'cpr', false);
system.stepFunction =@ stepVOTcW;
system.getEquations =@ eqsfiVOTcW;
system.updateState  =@ updateStateVOTcW;
%system.stepFunction =@ stepBlackOilTemp;
%system.getEquations =@ eqsfiBlackOilTempMod;
%system.updateState  =@ updateStateBlackOilTempMod;
system.fluid = fluid;       
system.nonlinear.use_ecltol = false;
system.nonlinear.tol = 5e-3;

%% initialize

ipress     = 5.101325e6 * Pascal; % Initial pressure
itemp      = 34 + 273.15;    % Initial temperature
ir = 1.25 * kilogram / second; % Injection rate 
injtemp = itemp; % Injected temperature

% Hydrostatic pressure function (of depth z)
grav = gravity;
grav = grav(3);
topPressure = ipress;
pressure = @(z) topPressure - fluid.rhoOS * (depth-z)*grav;

state.pressure = pressure(G.cells.centroids(:,3));
state.s = zeros(G.cells.num,3);
state.s(:,2) = 1;
state.s(:,1) = 0;
state.T = repmat(itemp, size(G.cells.centroids(:,3)));

if (system.activeComponents.disgas)
    state.rs = zeros(G.cells.num,1); 
else
    state.rs = 0;
end

state.rv = 0; % or 

%% The Well
W=[];
% wc = findClosestCell(G,400,500,0) + (0:G.cartDims(3)-1)'* (G.cartDims(1)) * (G.cartDims(2));
% wc_map = map(wc);
% wc_map = wc_map(wc_map>0);
% wc_map =  wc_map( G.cells.centroids(wc_map,3) > (layers(3)) );

ir_v = ir ./ fluid.rhoGS;
W = addWell(W, G, rock,  inj,     ...
                     'Type', 'rate', 'Val', ir_v,'refDepth',depth, ...
                     'Radius', 0.15, 'Name', 'I1','Comp_i',[0 0 1],'sign',1);
                 
% figure(2), clf
% plotGrid(G), view(-20,20)
% plotWell(G,W);

% ir_vO = ir ./ fluid.rhoOS;

%state.s(wc_map,1) = 0.01;
%state.s(wc_map,2) = 0.99;


%% BC
% G.faces.tag(G.faces.centroids(:,1) == 0,:) = -1; 
% G.faces.tag(G.faces.centroids(:,1) == Lx,:) = -2;
% G.faces.tag(G.faces.centroids(:,2) == 0,:) = -3; 
% G.faces.tag(G.faces.centroids(:,2) == Ly,:) = -4; 
rightside = G.faces.centroids(:,1) > (Ix(end) - largeEps);
bottomside = G.faces.centroids(:,3) > (Iz(end) - largeEps);
G.faces.tag(rightside) = -2;
G.faces.tag(bottomside) = -6;

bc = [];
bc = addBC(bc, find(rightside | bottomside),...
        'pressure', pressure(G.faces.centroids(rightside | bottomside,3) ) );
    
% bc=bc2ADbc(G,bc);
system.bc = bc;

for i=1:numel(W)
    % black oil relatetd
   W(i).bhpLimit = inf;
   if(W(i).sign>0)
       W(i).lims=struct('bhp',inf','rate',inf,'orat',inf,'wrat',inf,'grat',inf,'lrat',inf);
   else
       W(i).lims=struct('bhp',-inf','rate',-inf,'orat',-inf,'wrat',-inf,'grat',-inf,'lrat',-inf);
   end
   %W(i).refDepth = G.cells.centroids(W(i).cells(1),3);
   W(i).T = '';
   %W(i).dZ= 0 * ones(numel(W(i).cells),1);
   
end
W(1).T = injtemp;

% % W = struct([]);
% src = struct([]);
% src = addSource(src, inj,repmat(ir_v,size(inj)),'sat',[0,0,1]);
% src.fullscale = sparse(inj,1:numel(inj),1,G.cells.num,size(src.cell,1), 2*size(src.cell,1));
% src.resized = src.fullscale';
% system.src = src;

bcT = [];
bcT = addBCT(bcT, find(rightside | bottomside), 'temperature', itemp);
% bcT=bc2ADbc(G,bcT);
system.bcT = bcT;


%% Thermal part
fake_rock.perm = 1.0*ones(G.cells.num,1); % J / s*m*K
T = computeTrans(G,fake_rock);
Trans=1./accumarray(G.cells.faces(:,1),1./T,[G.faces.num,1]);
% Trans(bottomside | rightside) = Trans(bottomside | rightside)*1e-16;
internal=all(G.faces.neighbors>0,2);

system.s.T_r = Trans(internal);
system.s.T_r_all = Trans;
%system.s.iT = 33.6 + 273.15;
% pvmult = ones(G.cells.num,1);
% pvmult(union(bottom,right)) = 1e4;
% fluild.pvMultR = @(p) pvmult ;
%% schedule

endTime = 40 * day;
dtmin = (1e-6) * day;
dtmax = (1e-2) * day;
nsub = log(dtmax/dtmin);
substeps = dtmin * exp(linspace(0,nsub,1000));  % increasing sequence from dtmin to dtmax
nsteps = ceil((endTime-sum(substeps))/dtmax);
dtmax = (endTime-sum(substeps))/nsteps;
dt = [0; substeps'; dtmax*ones(nsteps,1)];
nsteps = numel(dt);

mrst_schedule.step = struct('control',ones(nsteps,1),'val',dt);

mrst_schedule.control(1).W = W;

if restart>0
    load(['state',sprintf('%05.0f',restart)]);
else
    delete(fullfile(outputdir,'*')) 
end
%mrst_schedule = struct('step',struct('control',ones(nsteps,1),'val',dt));
%mrst_schedule.control.W=W;
%mrst_schedule.W = {W};

%% RUN
[wellstates,states, iter] = runScheduleADI(state, G, rock, system, mrst_schedule,...
    'writeOutput',true,'outputdir',outputdir,'force_step',false);

%  restart = nsteps;
%% OUTPUT
%  See separate script files <Bielinski06_output*.m>



