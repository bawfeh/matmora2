%% Example by A. Bielinski (2006)

mrstModule add ad-fi deckformat ad-props
% workspace_path = '~/workspace/mrst-thermal/';
% addpath(genpath([workspace_path,'thermal/util']))
% addpath(genpath([workspace_path,'thermal/simulator']))
addpath(genpath('~/Documents/MATLAB/thermal'))

%% INPUT
clc
clear

load('axes4');  % xx, yy, zz, depth, Ix, right, bottom, inj 

Iz = Ix + depth; Iy = yy'; % Physical dimensions of reservoir

mrstVerbose(1);
outputdir = '~/Documents/MATLAB/thermal/examples/outputT307axes4';
saveasdir = '~/Documents/Notes/ECMOR14/Bielinski06T307axes4days40';
format = 'eps';
editflag = false;

perm    = 1.0 * darcy;            % Permeability 
phi        = 0.2;             % Porosity

nx = size(xx,1)-1;
ny = size(yy,1)-1;
nz = size(zz,1)-1;

largeEps = 1e-7;

gravity('on')

 restart = 0; 

%% The grid

G = tensorGrid(xx,yy,zz,'depthz',repmat(depth,nx+1,ny+1));
G = computeGeometry(G);
nc_tot = G.cells.num;
rock.perm=perm*ones(G.cells.num,1);
rock.poro=phi*ones(G.cells.num,1);
%%
% figure(1), clf
% plotGrid(G, 'FaceColor', 'yellow', 'EdgeAlpha', 0.5), view(-20,20)
% axis equal

%% OUTPUT
eos = [];
Type = 'simple';
Name = 'SW';
% load state at end time
load([outputdir,'/state_',Name,'.mat'])
%% The fluid
fluid = getCO2Brine('fluidprop','corey-brooks','pvt',Type,'eos',eos,...
    'Pe',1e3, 'salinity', 0.0, 'cR',1.9875e6,'rho',[1000 1182 479],'n',[1 1 1]);
fluid_simple = initSimpleADIFluid( 'mu', [1 71.85 3.950]*1e-5, 'rho',[1000 1000 479]);
fluid.muO = @(p,T) fluid_simple.muO(p,T);
fluid.bO = @(p,T) fluid_simple.bO(p,T);

%% 
rightside = G.faces.centroids(:,1) > (Ix(end) - largeEps);
bottomside = G.faces.centroids(:,3) > (Iz(end) - largeEps);

[X,Z] = meshgrid(G.faces.centroids(bottomside,1),G.faces.centroids(rightside,3));
X = X';
Z = -Z';

%%  Saturations
maxS = max(state.s(:,3));
minS = min(state.s(:,3));
cstep = min(0.05,maxS/2);
clevs = minS:cstep:maxS;
figure(1)
clf 
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
set(hc,'ticklength',2*get(hc,'ticklength'))
set(get(hc,'xlabel'),'String','S_{ CO_{2}}')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
    S = reshape(state.s(:,3),size(X));
    contourf(X,Z,S,clevs), drawnow
    axis([0 500 -1000 -500])
%     contourf(X,Z,S,clevs,'LineColor', 'none'), drawnow
set(gca,'YTickLabel', -get(gca,'YTick'))
savefig([outputdir,'/sat_',Name,'.fig'])
if editflag
    box on
    set(gca,'ticklength',2*get(gca,'ticklength'))
    set(gca,'XMinorTick','on')%set(gca,'XMinorTick','on','YMinorTick','on')
    saveas(gcf,[savesdir,'/sat_',Name],format)
end

%% Temperature
maxT = max(state.T);
minT = min(state.T); 
% cstep = 1;
% clevs = round(minT:cstep:maxT);
% clevs = linspace(minT,maxT,11);
% clevs = floor(clevs*10)/10;
clevs = minT:0.1:maxT;
clevs = unique([clevs,maxT]);
clevs = floor(clevs*10)/10;

figure(2)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs(1:2:end))
set(get(hc,'xlabel'),'String','T [K]')
axis tight
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
    T = reshape(state.T,size(X));
    contourf(X,Z,T,clevs), drawnow
    axis([0 500 -1000 -500])
%     contourf(X,Z,T,clevs,'LineColor', 'none'), drawnow
ytick = get(gca,'YTick');
set(gca,'YTickLabel', -ytick)
savefig([outputdir,'/temp_',Name,'.fig'])
if editflag
    box on
    set(gca,'ticklength',2*get(gca,'ticklength'))
    set(gca,'XMinorTick','on')
    saveas(gcf,[savesdir,'/temp_',Name],format)
end

%% Pressure (wetting-phase)
p = state.pressure;% + fluid.pcOG(state.s(:,2));
maxP = max(p)/mega;
minP = min(p)/mega;
cstep = 0.5;
start = floor(minP)+cstep;
fin = floor(maxP);
if (fin+cstep<maxP), fin = fin+cstep; end
if (start>minP), start = start-cstep; end
% clevs = unique([minP,start:cstep:fin,maxP]);
clevs = unique(start:cstep:fin);

figure(3)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs)
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
    P = reshape(p/mega,size(X));
    contourf(X,Z,P,clevs), drawnow
%     contourf(X,Z,P,clevs,'LineColor', 'none'), drawnow
    axis([0 500 -1000 -500])
set(gca,'YTickLabel', -get(gca,'YTick'))
savefig([outputdir,'/press_',Name,'.fig'])
if editflag
    box on
    set(gca,'ticklength',2*get(gca,'ticklength'))
    set(gca,'XMinorTick','on')
    saveas(gcf,[savesdir,'press_',Name],format)
end

%% Density
p = state.pressure + fluid.pcOG(state.s(:,2));
rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
minR = min(rhoG); maxR= max(rhoG);
rhoG(state.s(:,3)<largeEps) = 0;

% clevs = linspace(minR,maxR,20);
cstep = 50;
start = floor(minR/100)*100+cstep;
fin = floor(maxR/100)*100;
if (start<minR), start = start+cstep; end
if (fin>maxR), fin = fin - cstep; end
% clevs = round([minR, start:cstep:fin, maxR]);
clevs = round([start:cstep:fin, fin+cstep]);
clevs =unique([0,clevs]);

figure(4)
clf
axis([0 500 -1000 -500])
xlabel('x [m]'); ylabel('z [m]');
hc = colorbar('v'); 
caxis([clevs(1),clevs(end)])
set(hc,'YTick',clevs)
% set(hc,'YTickLabel',clevs)
colormap jet
set(gca,'Nextplot','replacechildren','Visible','on')
        R = reshape(rhoG,size(X));
        contourf(X,Z,R,clevs), drawnow
        axis([0 500 -1000 -500])
set(gca,'YTickLabel', -get(gca,'YTick'))
savefig([outputdir,'/den_',Name,'.fig'])
if editflag
    box on
    set(gca,'ticklength',2*get(gca,'ticklength'))
    set(gca,'XMinorTick','on')
    saveas(gcf,[savesdir,'/den_',Name],format)
end

%% Phase diagrams
figure(5)
clf
hold on
p = state.pressure + fluid.pcOG(state.s(:,2)); 
 P = reshape(p,size(X))'/mega;
%  rhoG = fluid.bG(p,state.T) * fluid.rhoGS;
%  R = reshape(rhoG,size(X))';
 T = reshape(state.T,size(X))';
 S = reshape(state.s(:,3),size(X))';
 for i = 2:size(T,2)
     bool = (S(:,i)>1e-12);
    if all(~bool), break; end
    plot(T(bool,i),P(bool,i),'b'), drawnow
 end
 bool = (S(:,1)>1e-12);
 plot(T(bool,1),P(bool,1),'r--'), drawnow
 xtick = get(gca,'XTick');
 plot(xtick,7.83*ones(size(xtick)),'k')
% Along the horizontal line (at the injection level)
k = find(Z(1,:)<=-900,1,'first');
bool = (S(k,:)>1e-12);
plot(T(k,bool),P(k,bool),'g')
savefig([outputdir,'/phaseDiags.fig'])
if editflag
    box on
    set(gca,'ticklength',2*get(gca,'ticklength'))
    set(gca,'XMinorTick','on')
    saveas(gcf,[savesdir,'/phaseDiags'],format)
end







